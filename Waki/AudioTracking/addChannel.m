function suce = addChannel(channel_name,frequencies)
suce = 1;
%processed
	connection = database_connect();		
		QUERY = ['insert into channels (channel_name) values (''' channel_name ''')'];	
		msg = exec(connection,QUERY);		
		if length(msg.Message) == 0
		
			QUERY = ['SELECT LAST_INSERT_ID()'];
			[insertedId]=fetch(connection,QUERY);
			lastInsertedId = cell2mat(insertedId(1));	
			
			for i=1:length(frequencies)				
				QUERY = ['insert into channel_frequencies (channel_id,frequency) values (''' num2str(lastInsertedId) ''',''' char(frequencies(i)) ''')'];					
				exec(connection,QUERY);
			end

		else
			suce = 0;
		end
	close(connection);  
	