function RegWin()
%processed
	global mainWindow;
	global ET_filePath;
	global ET_title;
	global ET_singer;
	global ET_musician;
	global ET_author;
	global L_loading;
	global B_add;
	global B_cancel;

	mainWindow = figure('Color',[1 1 1],'Position',[420 150 650 370],'resize','of');

	mainTitle = uicontrol('Style','text','String','Songs Registration','ForegroundColor',[.54 .59 .99],'backg',[1 1 1],'pos',[200 260 260 90],'fonts',16,'FontWeight','bold');
	L_audioFile = uicontrol('Style','text','String','Select a Audio Song (mp3)','backg',[1 1 1],'pos',[0 190 260 90],'fonts',10);
	ET_filePath = uicontrol('Style','edit','backg',[1 1 1],'pos',[245 263 300 20],'fonts',10,'Enable','off');
	B_browse = uicontrol('Style','pushbutton','String','Browse','Callback',{@browseFile,ET_filePath},'pos',[550 263 70 20]);

	L_title = uicontrol('Style','text','String','Song Title','backg',[1 1 1],'pos',[-49 160 260 90],'fonts',10);
	ET_title = uicontrol('Style','edit','backg',[1 1 1],'pos',[245 233 300 20],'fonts',10,'HorizontalAlignment','left');

	L_singer= uicontrol('Style','text','String','Singer Name','backg',[1 1 1],'pos',[-40 130 260 90],'fonts',10);
	ET_singer = uicontrol('Style','edit','backg',[1 1 1],'pos',[245 203 300 20],'fonts',10,'HorizontalAlignment','left');

	L_musician = uicontrol('Style','text','String','Musician Name','backg',[1 1 1],'pos',[-33 100 260 90],'fonts',10);
	ET_musician = uicontrol('Style','edit','backg',[1 1 1],'pos',[245 173 300 20],'fonts',10,'HorizontalAlignment','left');

	L_author = uicontrol('Style','text','String','Author Name','backg',[1 1 1],'pos',[-40 70 260 90],'fonts',10);
	ET_author = uicontrol('Style','edit','backg',[1 1 1],'pos',[245 143 300 20],'fonts',10,'HorizontalAlignment','left');

	B_add = uicontrol('Style','pushbutton','String','Add Song','Callback',@getCollection,'pos',[215 80 100 30]);
	B_cancel = uicontrol('Style','pushbutton','String','Cancel','Callback',@closeWin,'pos',[335 80 100 30]);

	L_loading = uicontrol('Style','text','backg',[1 1 1],'pos',[0 45 650 20],'fonts',10,'ForegroundColor',[1 0 0],'FontWeight','bold');

function browseFile(hObject,eventdata,myObj)
	[FileName,PathName] = uigetfile('*.mp3','Select the Audio File');
	if PathName ~= 0
		set(myObj,'String',[PathName FileName ]);
	end

function getCollection(hObject,eventdata)		
	global ET_filePath;
	global ET_title;
	global L_loading;
	global ET_singer;
	global ET_musician;
	global ET_author;
	global B_add;
	global B_cancel;

	set(L_loading,'String','Adding...');	
	if length(get(ET_filePath,'String')) == 0	
		set(L_loading,'String','Please Select a Song !');
		return;
	end
	if length(get(ET_title,'String')) == 0	
		set(L_loading,'String','Song Title is Required Field !');
		return;
	end	
	sendArry =[];
	MD = metaData;
	MD.title = get(ET_title,'String');
	MD.author = get(ET_author,'String');
	MD.singer = get(ET_singer,'String');
	MD.musician = get(ET_musician,'String');
	sendArry{1,1} = get(ET_filePath,'String');	
	fff=get(ET_filePath,'String');
	if length(MD.author) == 0
		MD.author = 'Unknown';
	end
	
	if length(MD.singer) == 0
		MD.singer = 'Unknown';
	end

	if length(MD.musician) == 0
		MD.musician = 'Unknown';
	end
	sendArry{1,2} = MD;
	set(B_add,'Enable','off');
	set(B_cancel,'Enable','off');	
	Reg_song(sendArry,fff); %external function one proceesed	
	set(B_add,'Enable','on');
	set(B_cancel,'Enable','on');
	set(L_loading,'ForegroundColor',[0 .47 0]);
	set(L_loading,'String','Completed');

function closeWin(hObject,eventdata)
	global mainWindow;
	close(mainWindow);
	homeWin();


