function directMatch(path)
	if isdir(path)	
		%fileName = regexp(path,'\','split');	
		allFiles = dir(path);
		for processFile = 1 : length(allFiles)
			if strcmp(allFiles(processFile).name,'.')==0 && strcmp(allFiles(processFile).name,'..') == 0			
				if isdir([path '\' allFiles(processFile).name])
					directMatch([path '\' allFiles(processFile).name]);
				else					
					title = regexp(allFiles(processFile).name,'\.','split');	
					if strcmp(char(title(2)),'wav') || strcmp(char(title(2)),'WAV')		
						disp(['processing : ' path,'\',allFiles(processFile).name]);
						matchEach([path,'\',allFiles(processFile).name],1);	
					elseif strcmp(char(title(2)),'mp3') || strcmp(char(title(2)),'MP3')
						disp(['processing : ' path,'\',allFiles(processFile).name]);
						matchEach([path,'\',allFiles(processFile).name],0);							
					end						
				end
			end		
		end
	else	
		[path,name,extent] = fileparts(path);
		title = regexp(path,'\.','split');	
		if strcmp(char(extent),'.wav') || strcmp(char(extent),'.WAV')	
			disp(['processing : ' path '\' name]);
			matchEach([path '\' name],1);		
		elseif strcmp(char(extent),'.mp3') || strcmp(char(extent),'.MP3')
			disp(['processing : ' path '\' name]);
			matchEach([path '\' name],0);	
		end
	end


function matchEach(path,type)
	if type == 1
		[Data,SR] = wavread(path);
        disp(['sampleRate wave ' num2str(SR)]);
	else
		[Data,SR] = mp3read(path);
        disp(['sampleRate mp3 ' num2str(SR)]);
    end
    
	framed = frameing(Data,SR); %external function one processed
	myHead = framed;	
	connection = database_connect();
	previousSong = -1;
	numFr = 1;
	while(length(myHead.Data) ~= 0)
		disp(['Processing frame ' num2str(numFr)]);
		matchedPercent = 13;
		R = patternMatch(myHead.Data,SR); %external function three processed
			if ((size(R,1) ~= 0) && (length(R(R(:,3) > matchedPercent,:)) ~= 0)) || ((size(R,1) > 1) && ((R(1,3) - R(2,3)) > 6))				
				detected_song = R(1:3,:);
				detected_song
				recog = 1;				
				
                QUERY = ['insert into report_data(song_id) values (''' num2str(detected_song(recog,1)) ''')'];
				exec(connection,QUERY);
                disp(['A song is detected with id ' num2str(detected_song(recog,1))]);
% 					if previousSong == -1 || previousSong ~= detected_song(recog,1)
% 						allFrame = 1;							
% 						previousSong = detected_song(recog,1);
% 						myId = fopen('testData.txt', 'a+');
% 						
% 						Query = ['select title,singer from meta_data where meta_data.song_id = ' num2str(detected_song(recog,1))];
% 						[getHash]=fetch(connection,Query);
%                         QUERYmy = ['insert into report_data(song_id) values(' num2str(detected_song(recog,1))];
% 						exec(connection,QUERYmy);
%                         myId = fopen('testData.txt', 'a+');
% 						fwrite(myId,['actual clip -> ' path  '  matched song ->' char(getHash(1)) ' by ' char(getHash(2)) '  Score: ' num2str(detected_song(recog,3))]);
% 						fprintf(myId,'\n');
% 						fclose(myId);						
% 					end				
			end
	
		myHead = myHead.Next;
		myHead.Prev.delete;
		numFr = numFr + 1;
	end	
	myId = fopen('testData.txt', 'a+');	
	fprintf(myId,'\n');
	fclose(myId);	