function H = hashFromFeatures(L,S)

%size(L) 3795           4
	if nargin < 2
	  S = 0;
	end
	if length(S) == 1
	  S = repmat(S, size(L,1), 1);	% size(L,1) (rows) by 1 vector containing all value of S
	end
	%disp('********');
	%size(L(:,1))  3923           1
	H = uint32(L(:,1));
	F1 = rem(round(L(:,2)-1),2^8); %Remainder after division
	DF = round(L(:,3)-L(:,2)); %Frequence Different
	if DF < 0
	  DF = DF + 2^8;
	end
	DF = rem(DF,2^6);
	DT = rem(abs(round(L(:,4))), 2^6); %delta-time
	%H is size(L,1)*3 vector
	H = [S,H,uint32(F1*(2^12)+DF*(2^6)+DT)];