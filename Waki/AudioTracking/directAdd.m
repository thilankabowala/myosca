function directAdd()
    path='E:\\dd\\adarepudapu.mp3';
	startTime = clock;
	numSongs = addDirectory(path);
	ellapesTime = etime(clock,startTime);
	disp([num2str(numSongs) ' songs were added, total elapsed time is ' num2str(ellapesTime) ' S']);
	
function nSong = addDirectory(path)	
	numSong = 0;
	if isdir(path)
		fileName = regexp(path,'\','split');	
		allFiles = dir(path);
		for processFile = 1 : length(allFiles)
			if strcmp(allFiles(processFile).name,'.')==0 && strcmp(allFiles(processFile).name,'..') == 0
				if isdir([path '\' allFiles(processFile).name])
					addDirectory([path '\' allFiles(processFile).name]);
				else					
					title = regexp(allFiles(processFile).name,'\.','split');	
					if strcmp(char(title(2)),'mp3') || strcmp(char(title(2)),'MP3')		
						startTime = clock;
						addEach([path,'\',allFiles(processFile).name],char(title(1)),char(fileName(end)));	
						ellapesTime = etime(clock,startTime);
						
						myId = fopen('addingTime.txt', 'a+');
						fwrite(myId,['Total elapsed time is ' num2str(ellapesTime) ' s']);
						fprintf(myId,'\n');
						fclose(myId);					
						
						disp(['Total elapsed time is ' num2str(ellapesTime) ' s']);
						numSong = numSong + 1;
					end						
				end
			end		
		end
	else
		disp('Invalid Directory Path');
	end
	nSong = numSong;
	
function addEach(path,title,singer,musician,author)
	sendArry =[];
	MD = metaData;
	MD.musician = 'Unknown';
	MD.author = 'Unknown';			
	
	if nargin >= 2
		MD.title = title;
	end

	if nargin >= 3
		MD.singer = singer;
	end
	
	if nargin >= 4
		MD.musician = musician;
	end

	if nargin >= 5
		MD.author = author;
	end
	sendArry{1,1} = path;
	sendArry{1,2} = MD;	
	Reg_song(sendArry);
	disp(['Added the song => ',path]);