function A = recProcThread(globFrequencyId)
clear global process;
global process;

 myhandles = guidata(gcbo);
 isSkip = myhandles.noFilter;

% delete('5MinClips/*_clip.wav');
delete('stop/*stop.txt');

matlabpool(2)
try
	spmd
		if(labindex == 1)%processing thread.....	
		process = mySystem(process);		
			currantClipIndex = 0;
			numPauses = 1;		
			previousId = -1;
			allFrm = 0;
			LastInsID = 0;
			while 1		
				if mod(numPauses,2) == 1
					disp(['Thread 1: Waiting for clip ' num2str(numPauses*10) ' seconds']);
				end
				currantClipIndex = mod(currantClipIndex,100);
				currantClip = fopen(['5MinClips/' num2str(currantClipIndex) '_clip.wav'],'r');									
				stopFile = fopen('stop/stop.txt','r');				
				if stopFile ~= -1						
					fclose(stopFile);
					error('Stoped');	
				end
				
				if labProbe(2)
					disp('Thread 1: No input signal, radio may be Off, System is going to sleep');
					process = mySystem(process);
					labReceive(2);
					break;
				end			
				
				if currantClip == -1
					pause(10);	
					numPauses = numPauses + 1;
					continue;	
				else
					numPauses = 1;
				end	
				
				[previousId,allFrm,LastInsID] = proces(currantClipIndex,previousId,allFrm,LastInsID,globFrequencyId,isSkip);
				fclose(currantClip);		
% 				delete(['5MinClips/' num2str(currantClipIndex) '_clip.wav']);
				currantClipIndex = currantClipIndex + 1;
			end
		else
			currantRecIndex = 0;		
			while 1	
				currantRecIndex = mod(currantRecIndex,100);
				allSilent = Rec(currantRecIndex,isSkip);	
				if allSilent == 1				
					break;			
				end			
				currantRecIndex = currantRecIndex + 1;
			end
		end

	end
catch
	disp('Stoped by user !');
end

matlabpool close;
	
function [PS,allFrame,LastID] = proces(clipIndex,previousSong,passedFrm,LastID,globFrequencyId,isSkip)	
    disp('Thread 1: Start of processing....');
	verbose = 0;
    writeStat = 0;
	PS = previousSong;
	allFrame = passedFrm;
	matchedPercent = 10;
	maxClipSilences = 50; %@@@@@@@@@@@@%
	%maxClipSilences = 140; %@@@@@@@@@@@@%
	numDuraFrame = 2; %@@@@@@@ 7 for 280, multimplier of 40 @@@@@%
	numNonSong = 0;
	iter = 0;
	frameLength = 40;
	
	[Data,SR] = wavread(['5MinClips/' num2str(clipIndex) '_clip.wav']);
	framed = frameing(Data,SR); %external function one processed
	myHead = framed;	
	connection = database_connect();

	while(length(myHead.Data) ~= 0)
        
        stopFile = fopen('stop/stop.txt','r');				
		if stopFile ~= -1						
            fclose(stopFile);
			error('Stoped');	
		end
		matchedPercent = 13;
		originalFrame = mean(myHead.Data,2);
		originalFrame(originalFrame < 0)=0;
	
		if iter == 4	%%%%%%%%%%%%%%%middle index%%%%%%%%%%%%%%%%%%%%%
			middle = myHead.Data;
			middleSR = SR;
		end
		clipSilences = maxClipSilences;
		if isSkip == 11 || isSkip == 12
			clipSilences = numSilences(myHead.Data,SR,mean(originalFrame)+0.001); %external function two processed
		end
		if verbose
			disp(['Thread 1: clipSilences ' num2str(mean(clipSilences))]);			
		end
		
		if clipSilences <= maxClipSilences
			disp(['Thread 1: A clip is detected as a song ']);
			isSpeedAdjest = 'null';
			R = patternMatch(myHead.Data,SR); %external function three processed
			if mod(isSkip,10) == 1
                %speed adjustment block
                if(size(R,1) == 0) || (length(R(R(:,3) > matchedPercent,:)) == 0)
                    disp('Time adjustment involve...');
                    matchedPercent = 9;
                    for speedAdj = 1:1:6
                        stopFile = fopen('stop/stop.txt','r');				
                        if stopFile ~= -1						
                            fclose(stopFile);
                            error('Stoped');	
                        end
                        adjestment = 0;
                        if mod(speedAdj,2) == 0
                            adjestment =(speedAdj/2)*500;
                        else
                            adjestment = -1*((speedAdj+1)/2)*500;
                        end
                        R = patternMatch(myHead.Data,SR+adjestment); %external function three processed					
                        if(size(R,1) ~= 0) && (length(R(R(:,3) > matchedPercent,:)) ~= 0)
                            isSpeedAdjest = 1;
                            break;
                        end					
                    end
                end
            end
            
			if ((size(R,1) ~= 0) && (length(R(R(:,3) > matchedPercent,:)) ~= 0)) || ((size(R,1) > 1) && ((R(1,3) - R(2,3)) > 6))
				detected_song = R(1:3,:);
				disp('Detected Song Info => ');
                disp(detected_song(1:1,:));
				recog = 1;					
				%disp(['Thread 1: A song is detected with id ' num2str(detected_song(recog,1))]);
                if previousSong == -1 || previousSong ~= detected_song(recog,1)
					allFrame = 1;
					PS = detected_song(recog,1);
					previousSong = detected_song(recog,1);
					currentT = datestr(now,'yyyy-mm-dd HH:MM:SS');
					currentTNum = datenum(currentT);							
					frameNum = numDuraFrame - iter -1;
					frameNum = round(frameNum*frameLength + frameLength/recog); %can be overlap, it is error...
					beginTime = addtodate(currentTNum, -1*frameNum, 'second');										
					if isSpeedAdjest == 1
						QUERY = ['insert into report_data(song_id,begin_time,end_time,score,frequency_id) values (' num2str(detected_song(recog,1)) ','''  datestr(beginTime,'yyyy-mm-dd HH:MM:SS') ''',null,1,''' num2str(globFrequencyId) ''')'];	
					else
						QUERY = ['insert into report_data(song_id,begin_time,end_time,score,frequency_id) values (' num2str(detected_song(recog,1)) ','''  datestr(beginTime,'yyyy-mm-dd HH:MM:SS') ''',null,null,''' num2str(globFrequencyId) ''')'];	
					end
												
					exec(connection,QUERY);
					QUERY2 = ['SELECT LAST_INSERT_ID()'];							
					[insertedId]=fetch(connection,QUERY2);
					LastID = cell2mat(insertedId(1));
					QUERY2 = ['update report_data set end_time = DATE_ADD(begin_time, INTERVAL ' num2str(frameLength) ' second) where detected_id = ' num2str(LastID)];							
					exec(connection,QUERY2);
					wavwrite(myHead.Data,SR,['matchedSong/' num2str(LastID) '.wav']);                  
                            
                elseif previousSong == detected_song(recog,1)
                    allFrame = allFrame + 1;													
                    QUERY2 = ['update report_data set end_time = DATE_ADD(begin_time, INTERVAL ' num2str(allFrame*frameLength) ' second) where detected_id = ' num2str(LastID)];
                    exec(connection,QUERY2);							
                end
					
            else
                myWaveWrite(myHead.Data,SR,'detectedSongClips'); %external function four processed
				if PS ~= -1
					allFrame = allFrame + 1; %if poluted song
				end
            end
            
            if writeStat
				myId = fopen('tempTest.txt', 'a+');
				fwrite(myId,['detectedSongClips ' num2str(clipSilences)]);
				fprintf(myId,'\n');
				fclose(myId);
            end
				
		else
			if PS ~= -1
				allFrame = allFrame + 1; %if conversation
			end
			disp(['Thread 1: A clip is filtered as a non-song ']);
			numNonSong = numNonSong + 1;
            if writeStat
                myId = fopen('tempTest.txt', 'a+');
                fwrite(myId,['nonSongObjects ' num2str(clipSilences)]);
                fprintf(myId,'\n');
                fclose(myId);
            end
			myWaveWrite(myHead.Data,SR,'nonSongObjects',iter); %external function five processed
        end		
		myHead = myHead.Next;
		myHead.Prev.delete;
		iter = iter + 1;
	end	

	if numNonSong > 6 %%%%%%%%%%%%%%% max frame %%%%%%%%%%%%%%%%%%%%%
        matchedPercent = 13;
		R = patternMatch(middle,middleSR); %external function four processed
		if ((size(R,1) ~= 0) && (length(R(R(:,3) > matchedPercent,:)) ~= 0)) || ((size(R,1) > 1) && ((R(1,3) - R(2,3)) > 6))		
            detected_song = R(1:3,:);
			disp('Detected Song Info => ');
            disp(detected_song(1:1,:));
			allFrame = 1;
			PS = detected_song(recog,1);
			previousSong = detected_song(recog,1);
			currentT = datestr(now,'yyyy-mm-dd HH:MM:SS');
			currentTNum = datenum(currentT);							
			frameNum = numDuraFrame - iter -1;
			frameNum = frameNum*frameLength + frameLength/recog; %can be overlap, it is error...
			beginTime = addtodate(currentTNum, -1*frameNum, 'second');
			QUERY = ['insert into report_data(song_id,begin_time,end_time,score,frequency_id) values (' num2str(detected_song(recog,1)) ','''  datestr(beginTime,'yyyy-mm-dd HH:MM:SS') ''',null,null,''' num2str(globFrequencyId) ''')'];							
			exec(connection,QUERY);
			
			QUERY2 = ['SELECT LAST_INSERT_ID()'];							
			[insertedId]=fetch(connection,QUERY2);
			LastID = cell2mat(insertedId(1));
			
			QUERY2 = ['update report_data set end_time = DATE_ADD(begin_time, INTERVAL ' num2str(frameLength) ' second) where detected_id = ' num2str(LastID)];							
			exec(connection,QUERY2);	
            wavwrite(myHead.Data,SR,['matchedSong/' num2str(LastID) '.wav']);
		end
	end	
	disp('Thread 1: End of processing....');
    close(connection);

function T = Rec(recIndex,isSkip)

	verbose = 0;
	bufferDuration = 80; %%%%%%%%%%%%%%%%%%%%2%%%%%%%%%%%%%%%%	
	%bufferDuration = 280; %%%%%%%%%%%%%%%%%%%%7%%%%%%%%%%%%%%%%
	sampleRate = 48000;
	fiveMinSilents = 2200; %two frame 3950
	recObj = audiorecorder(sampleRate, 16, 2);
    
	disp('Thread 2: Start Buffering...');
	recordblocking(recObj,bufferDuration); 
	myRecording = getaudiodata(recObj);
	silences = 0;
	if isSkip == 11 || isSkip == 12
		silences = numSilences(myRecording,sampleRate,0.010); %external function five processed
	end
	if verbose
		disp(['TSilent ' num2str(silences)]);		
	end
	if silences > fiveMinSilents
		T = 1;			
		labSend(1,1);
        %should wait untill the remaing clips are processed
		disp('Thread 2: Stop Buffering...');
	else
		T = 0;
		wavwrite(myRecording,sampleRate,['5MinClips/' num2str(recIndex) '_clip.wav']);
      
       
	end

function process = mySystem(process)
%global process;
	if (~isempty(process))		
		process.destroy();
	else
		runtime = java.lang.Runtime.getRuntime();
		process = runtime.exec('java stopProcess');
    end

