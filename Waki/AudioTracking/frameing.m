function framedAry = frameing(songData,srt)
%processed
	frameSize = 40;
	minFrameSize = 30;

	recPerFrame = srt*frameSize;
	recPerMinFrame = srt*minFrameSize;
	beginIndex = 1;		
	ins = myLL();	
	no = getHead(ins); 
	if length(songData) > recPerFrame 
		for i = 0 : recPerFrame : length(songData)		
			if (length(songData)-beginIndex+1) >= recPerFrame
				[ins,no] = addToHead(ins,no,songData(beginIndex:beginIndex+recPerFrame-1,:));		
				beginIndex = beginIndex + recPerFrame;
			elseif (length(songData)-beginIndex+1) >= recPerMinFrame
				[ins,no] = addToHead(ins,no,songData(beginIndex:beginIndex+recPerMinFrame-1,:));		
				beginIndex = beginIndex + recPerMinFrame;
			end		
		end
	elseif length(songData) > recPerMinFrame
		[ins,no] = addToHead(ins,no,songData);	
	end
	[ins,no]  = addToHead(ins,no);
	testHead = getHead(ins);
	framedAry = testHead;