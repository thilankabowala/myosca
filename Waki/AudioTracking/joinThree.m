function joinThree(path)
		allFiles = dir(path);
		AllInOne = zeros(1,2);
		SR1=0;
		for processFile = 1 : length(allFiles)	
			
			if strcmp(allFiles(processFile).name,'.')==0 && strcmp(allFiles(processFile).name,'..') == 0	
				if isdir([path '\' allFiles(processFile).name])
					joinThree([path '\' allFiles(processFile).name]);
				else	
						[path,name,extent] = fileparts([path,'\',allFiles(processFile).name]);
						if strcmp(char(extent),'.mp3') || strcmp(char(extent),'.MP3')	
							disp(['Processing the file ' path,'\',allFiles(processFile).name]);
							[AudioData,SR] = mp3read([path,'\',allFiles(processFile).name]);
							if SR ~= 44100
								disp(['Original Rate ' num2str(SR)]);
								AudioData = resample(AudioData,44100,SR);
							end
							AllInOne = [AllInOne;AudioData];
							SR = (44100/SR)*SR;
							if SR1 ~= 0 && SR1 ~= SR
								disp(['Error : ' path,'\',allFiles(processFile).name ' ' num2str(SR) ' ' num2str(SR1)]);
							end
							
							SR1 = SR;
						end
				end
			
			end
		end
		wavwrite(AllInOne,44100,[path '\' 'allInOne.wav']);
