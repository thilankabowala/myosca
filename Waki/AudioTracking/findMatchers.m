function R = findMatchers(H)
%no external functions
	if size(H,2) == 3
	  H = H(:,[2 3]); %skip first column (song ID)
	end

	if min(size(H))==1
	  H = [zeros(length(H),1),H(:)];
	end

	TIMESIZE=16384;
	Rmax = 0;	
	connection = database_connect();
if size(H,1) == 0
R=[];
return;
end
	hashIds = [(H(1,2) + 1)];
	
	for i = 2:length(H)
		hash = H(i,2);
		hashIds = [hashIds;(hash + 1)];
	end
	hashIds = unique(hashIds);
	finalQ = ['insert into hitted_song_temp(hash_id) values (' num2str(hashIds(1)) '),'];
	for j = 2:length(hashIds)-1
		finalQ = [finalQ '(' num2str(hashIds(j)) '),'];
	end
	finalQ = [finalQ '(' num2str(hashIds(length(hashIds))) ')'];

	startTime = clock;
	exec(connection,'delete from hitted_song_temp');
	exec(connection,finalQ);
	[getHash]=fetch(connection,'select hash_value,hash_id from hitted_song where hash_id in(select hash_id from hitted_song_temp)');
	ellapesTime = etime(clock,startTime);
	disp(['Total elapsed time is ' num2str(ellapesTime) ' s']);
	
	R = zeros(length(getHash),3);
	for i = 1:length(getHash)
		htcol_db = str2double(getHash(i));
		songs = floor(htcol_db/TIMESIZE);		
		R(Rmax+[1:1],:) = [songs, 2, repmat(double(hash),1,1)]; %create nentries # rows with <songID,DeltaTime,hash >
		Rmax = Rmax + 1;
	end
	close(connection);
	R = R(1:Rmax,:); %choping off empty rows
