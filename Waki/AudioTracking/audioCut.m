function audioCut(path)
	[AudioData,SR] = mp3read(path);
	[path,name,extent] = fileparts(path);
	base = 'E:\acedemic\4th_year\Project\Nishan\testCase\notExistParts';
	for clips = 2:8
		duration = clips*5;		
		AudioDataPart=AudioData(SR*20:SR*20+SR*duration,:);
		if ~isdir([base '\' num2str(duration) ' s'])
			mkdir([base '\'],[num2str(duration) ' s']);
		end
		wavwrite(AudioDataPart,SR,[base '\' num2str(duration) ' s' '\' name ' ' num2str(duration) '.wav']);
	end
