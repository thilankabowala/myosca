function [R,L] = patternMatch(D,SR,IX)
%ok

	if nargin < 3
		IX = 1;
	end

	dens = 20;

	if size(D,2) == 2
	  D = mean(D,2);
	end

	Lq = featureFinder(D,SR, dens); %external Function 1
	landmarks_hopt = 0.032;

	Lq = [Lq;featureFinder(D(round(landmarks_hopt/4*SR):end),SR, dens)]; %384 append at the end....
	Lq = [Lq;featureFinder(D(round(landmarks_hopt/2*SR):end),SR, dens)]; %768
	Lq = [Lq;featureFinder(D(round(3*landmarks_hopt/4*SR):end),SR, dens)];%1152
	% add in quarter-hop offsets too for even better recall

	Hq = unique(hashFromFeatures(Lq), 'rows'); %external Function 2
	Rt = findMatchers(Hq); %external Function 3

	nr = size(Rt,1);
	if nr > 0
		[utrks,xx] = unique(sort(Rt(:,1)),'first'); %unique value of first occorence utrks=unique tracks array, xx is indexes
		utrkcounts = diff([xx',nr]);

		[utcvv,utcxx] = sort(utrkcounts, 'descend');
		utcxx = utcxx(1:min(20,length(utcxx)));
   
		utrkcounts = utrkcounts(utcxx); %unwanted  
		utrks = utrks(utcxx);
  
		nutrks = length(utrks); %# tracks hitted
		R = zeros(nutrks,3);
		wholeHashes = 0;
		for i = 1:nutrks
			tkR = Rt(Rt(:,1)==utrks(i),:); % return song by song segments	
			[dts,xx] = unique(sort(tkR(:,2)),'first'); %delta time
			dtcounts = 1+diff([xx',size(tkR,1)]);
			[vv,xx] = max(dtcounts);

			R(i,:) = [utrks(i),sum(abs(tkR(:,2)-dts(xx(1)))<=1),0];
			wholeHashes = wholeHashes + R(i,2);
		end

		[vv,xx] = sort(R(:,2),'descend');
		R = R(xx,:);
  
		for i = 1:length(R)
			R(i,3) = (R(i,2)*100)/size(Hq,1);
		end
  
		maxrtns = 100;
		if size(R,1) > maxrtns
			R = R(1:maxrtns,:);
		end
	else
		R = []; 
	end
