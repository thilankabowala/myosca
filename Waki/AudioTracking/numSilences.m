function silences = numSilences(original,SR,cutOff)	
%processed
%cutOff = 0.039;
	original = mean(original,2);
	anal = original(1:round(SR/16):end); %SR/4 means take 4 record per second
	consequativeZeros = 8; %4 means silent 1/4 second
	silences = 0;
	previousIndex = 0;
	for itera = 1:1:8
		previousIndex = previousIndex+1;
		nextIndex = consequativeZeros;
		
		while nextIndex <= length(anal)
			if (sum(anal(previousIndex:nextIndex) < cutOff) == consequativeZeros) && (sum(anal(previousIndex:nextIndex) > -1*cutOff) == consequativeZeros)
				silences = silences + 1;
				previousIndex = nextIndex+1;
				nextIndex = nextIndex + consequativeZeros;				
			else
			previousIndex=previousIndex+1;
			nextIndex = nextIndex + 1;
			end
		end
	
	end 
	silences = (silences*100)/4;
	