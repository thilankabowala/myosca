function [L,S,T,maxes] = tempDet(D,SR,N)
	if nargin < 3;  N = 7;  end % 7 to get a_dec = 0.998

	f_sd = 30;
	a_dec = 1-0.01*(N/35);
	maxpksperframe = 5;
	hpf_pole = 0.98;
	targetdf = 31; 
	targetdt = 63; 	
	[nr,nc] = size(D);

	if nr > nc
	  D = D';
	  [nr,nc] = size(D);
	end
	if nr > 1
	  D = mean(D);
	  nr = 1;
	end

	targetSR = 8000;
	if (SR ~= targetSR)
	  D = resample(D,targetSR,SR);
	end

	fft_ms = 64;
	fft_hop = 32;
	nfft = round(targetSR/1000*fft_ms);
	nfft
	nfft-round(targetSR/1000*32)
	S = abs(specgram(D,nfft,targetSR,nfft,nfft-round(targetSR/1000*32)));

	Smax = max(S(:));
	S = log(max(Smax/1e6,S));
	S = S - mean(S(:)); 
	S = (filter([1 -1],[1 -hpf_pole],S')');
	maxespersec = 30;

	ddur = length(D)/targetSR; %Duration in second
	nmaxkeep = round(maxespersec * ddur); %total # maxes
	maxes = zeros(3,nmaxkeep);
	nmaxes = 0;
	maxix = 0;
	s_sup = 1.0;
%{
AA = [1,2,3,4,5,6,67,54,4,6,6,4,6,4,5,6,7,8,5,6,67,78,8,4,8,9,9,9,9,8];
spread(AA,f_sd)'
return;
%}

	sthresh = s_sup*spread(max(S(:,1:min(10,size(S,2))),[],2),f_sd)';
	%sthresh is a vector
	T = 0*S;

	for i = 1:size(S,2)-1
	  s_this = S(:,i);
	  sdiff = max(0,(s_this - sthresh))';  
	  %size(sdiff) 1 257 always
	  sdiff = locmax(sdiff);
	  sdiff(end) = 0;  
	  [vv,xx] = sort(sdiff, 'descend');
	  xx = xx(vv>0);
	  nmaxthistime = 0;
	  for j = 1:length(xx)
		p = xx(j);
		if nmaxthistime < maxpksperframe
		  if s_this(p) > sthresh(p)
			nmaxthistime = nmaxthistime + 1;
			nmaxes = nmaxes + 1;
			maxes(2,nmaxes) = p;
			maxes(1,nmaxes) = i;
			maxes(3,nmaxes) = s_this(p);
			eww = exp(-0.5*(([1:length(sthresh)]'- p)/f_sd).^2);
			sthresh = max(sthresh, s_this(p)*s_sup*eww);
		  end
		end
	  end
	  T(:,i) = sthresh;
	  sthresh = a_dec*sthresh;
	end
	maxes2 = [];
	nmaxes2 = 0;
	whichmax = nmaxes;

	sthresh = s_sup*spread(S(:,end),f_sd)';
	for i = (size(S,2)-1):-1:1
	  while whichmax > 0 && maxes(1,whichmax) == i
		p = maxes(2,whichmax);
		v = maxes(3,whichmax);
		if  v >= sthresh(p)
		  nmaxes2 = nmaxes2 + 1;
		  maxes2(:,nmaxes2) = [i;p]; 
		  eww = exp(-0.5*(([1:length(sthresh)]'- p)/f_sd).^2);
		  sthresh = max(sthresh, v*s_sup*eww);
		end
		whichmax = whichmax - 1;
	  end
	  sthresh = a_dec*sthresh;    
	end
	maxes2 = fliplr(maxes2);
	maxpairsperpeak=3;
	L = zeros(nmaxes2*maxpairsperpeak,4);

	nlmarks = 0;

	for i =1:nmaxes2
	  startt = maxes2(1,i); %time
	  F1 = maxes2(2,i);
	  maxt = startt + targetdt;
	  minf = F1 - targetdf;
	  maxf = F1 + targetdf;
	  matchmaxs = find((maxes2(1,:)>startt)&(maxes2(1,:)<maxt)&(maxes2(2,:)>minf)&(maxes2(2,:)<maxf));
	  if length(matchmaxs) > maxpairsperpeak
		matchmaxs = matchmaxs(1:maxpairsperpeak);
	  end
	  for match = matchmaxs
		nlmarks = nlmarks+1;
		L(nlmarks,1) = startt;
		L(nlmarks,2) = F1;
		L(nlmarks,3) = maxes2(2,match);  % frequency row
		L(nlmarks,4) = maxes2(1,match)-startt;  % time column difference
	  end
	end

	L = L(1:nlmarks,:);
	maxes = maxes2;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%return local maximas and others as zeros, if input is 1,2,4,2,4,6,83,69,3,6 out put is => 0,0,4,0,0,0,83,0,0,0
function Y = locmax(X)
	X = X(:)';
	nbr = [X,X(end)] >= [X(1),X]; %return boolean vector with 0 and 1,; 1 if current one is greter than previous one
	Y = X .* nbr(1:end-1) .* (1-nbr(2:end));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Y = spread(X,E)
	if nargin < 2; E = 4; end
  
	if length(E) == 1
	  W = 4*E;  
	  E = exp(-0.5*[(-W:W)/E].^2); % step size is 1 (default).
	end

	X = locmax(X);
	Y = 0*X;
	lenx = length(X); 
	maxi = length(X) + length(E);

	spos = 1+round((length(E)-1)/2);

	for i = find(X>0) %returns linear indices corresponding to the entries of X that are greater than 0 as a vector.
	  EE = [zeros(1,i),E];
	  EE(maxi) = 0;
	  EE = EE(spos+(1:lenx));
	  Y = max(Y,X(i)*EE);  
	end


