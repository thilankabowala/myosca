classdef mdata
	properties (GetAccess = private)
		title
		author
		musician
		singer
	end
	
	methods 
		function ret = setTitle(myobj2,localTitle)			
			myobj2.title = localTitle;
            ret = myobj2;
		end
		function ret = getTitle(myobj)
			ret=myobj.title;
		end
	end
end