function homeWin()
	global CB_clearHis;
	global L_msg;
	global B_Report;
	global B_close;
	global popup_freq;	
	%global globFrequencyId;
	%clear globFrequencyId
	
 	allChannels = getChannels();
 	allChannels = ['--Select a Channel--';allChannels];	

	homeWindow = figure('Color',[1 1 1],'Position',[430 150 450 390],'resize','of');

	myhandles = guihandles(homeWindow); 
	myhandles.noFilter = 12; 
	guidata(homeWindow,myhandles) 

	
	uicontrol('Style','text','String','Intelligent Song Tracking System','ForegroundColor',[.54 .59 .99],'backg',[1 1 1],'pos',[0 330 450 40],'fonts',16,'FontWeight','bold');
	
	B_addChannel = uicontrol('Style','pushbutton','String','Add Channel','Callback',{@addChannel,homeWindow},'pos',[50 270 155 50],'fonts',9);
	B_Reg = uicontrol('Style','pushbutton','String','Songs Registration','Callback',{@songReg,homeWindow},'pos',[250 270 155 50],'fonts',9);
	popup_channel = uicontrol('Style', 'popupmenu','String',allChannels,'Position', [250 205 155 20],'Callback', @ChannelChange);
	uicontrol('Style','text','String','Skip Filtering','backg',[1 1 1],'pos',[50 233 80 17],'fonts',10);
	CB_skipFilter = uicontrol('Style','checkbox','pos',[138 233 15 15],'backg',[1 1 1]);
	uicontrol('Style','text','String','Speed Adjustment','backg',[1 1 1],'pos',[224 233 160 17],'fonts',10);
	CB_skipSpeed = uicontrol('Style','checkbox','pos',[367 233 15 15],'backg',[1 1 1]);
	B_monitor = uicontrol('Style','pushbutton','String','Monitor Radio Channel','Callback',{@monitorChannel,B_Reg,B_addChannel,popup_channel,CB_skipFilter,CB_skipSpeed},'pos',[50 176 155 50],'fonts',9);
			
	popup_freq = uicontrol('Style','popupmenu','String',{'--Select a Frequency--'},'pos',[250 179 155 20],'HorizontalAlignment','left','Callback',{@frequenceChange,popup_channel});
	
	CB_genRep = uicontrol('Style','checkbox','pos',[352 134 20 20],'backg',[1 1 1]);
	B_Report = uicontrol('Style','pushbutton','String','Generate the Report','Callback',{@reportGen,CB_genRep},'pos',[50 105 155 50],'fonts',9);
	
	uicontrol('Style','text','String','Generate Report','backg',[1 1 1],'pos',[240 132 113 20],'fonts',10);
	uicontrol('Style','text','String','Clear History','backg',[1 1 1],'pos',[223 106 130 20],'fonts',10);
	CB_clearHis = uicontrol('Style','checkbox','pos',[352 107 20 20],'backg',[1 1 1]);
	B_close = uicontrol('Style','pushbutton','String','Close','Callback',{@closeApp,homeWindow},'pos',[200 56 55 30],'fonts',9);	
	L_msg = uicontrol('Style','text','String','','backg',[1 1 1],'pos',[0 26 450 20],'fonts',10,'ForegroundColor',[1 0 0],'FontWeight','bold');

function closeApp(hObject,eventdata,closeThis)	
	close(closeThis);
	
function songReg(hObject,eventdata,closeThis)	
	close(closeThis);
	RegWin(); %external Function one processed	
	
function addChannel(hObject,eventdata,closeThis)	
	close(closeThis);
	addChannelWin(); %external Function one processed	
	
function reportGen(hObject,eventdata,CB_genRep)
	global L_msg;
	global CB_clearHis;
	if get(CB_genRep,'Value') == 1
		set(L_msg,'String','Generating...');
		sucess = generateReport();	%external Function two processed
		if sucess == 0
			set(L_msg,'ForegroundColor',[1 0 0]);
			set(L_msg,'String','No History Data!');
			return;
		end
		set(L_msg,'ForegroundColor',[0 .47 0]);
		set(L_msg,'String','Report was generated successfully');

	end
	
	if get(CB_clearHis,'Value') == 1
		clearReportData(); %external Function three Processed
		set(L_msg,'ForegroundColor',[0 .47 0]);
		set(L_msg,'String','Clear Report History');
	end
	
	if get(CB_clearHis,'Value') == 1 && get(CB_genRep,'Value') == 1
		set(L_msg,'ForegroundColor',[0 .47 0]);
		set(L_msg,'String','Completed');
	end
	
	if (get(CB_clearHis,'Value') == 0) && (get(CB_genRep,'Value') == 0)
		set(L_msg,'ForegroundColor',[1 0 0]);
		set(L_msg,'String','Please Select an Option');
	end
	
function ChannelChange(hObject,eventdata)
	global popup_freq;
	global globFrequencyId;	
    str = get(hObject, 'String');
    val = get(hObject,'Value');	 	
	set(popup_freq,'Value',1);
	if strcmp(str{val},'--Select a Channel--') ~= 1
		myData = getFrequency(str{val});		
		freqs = myData(1:end,1);
		freqs = ['--Select a Frequency--';freqs];
		set(popup_freq,'String',freqs);
	else
		set(popup_freq,'String',{'--Select a Frequency--'});
		globFrequencyId = [];
	end

function frequenceChange(hObject,eventdata,popup_channel)	
	freStr = get(hObject, 'String');
    freVal = get(hObject,'Value');		
	channelStr = get(popup_channel, 'String');
    channelSVal = get(popup_channel,'Value');	
	global globFrequencyId;		
	if strcmp(freStr{freVal},'--Select a Frequency--') == 1
		globFrequencyId = [];
	else
		globFrequencyId = getFreqId(channelStr{channelSVal},freStr{freVal});
		globFrequencyId = cell2mat(globFrequencyId);
	end
	

	
function monitorChannel(hObject,eventdata,B_Reg,B_addChannel,popup_channel,skipFilter,Speed)
	speedVal = 12;
	if get(skipFilter,'Value') == 1 && get(Speed,'Value') == 1
	speedVal = 21;
	elseif get(skipFilter,'Value') == 0 && get(Speed,'Value') == 1
	speedVal = 11;
	elseif get(skipFilter,'Value') == 1 && get(Speed,'Value') == 0
	speedVal = 22;
	end

	myhandles = guidata(gcbo);
	myhandles.noFilter = speedVal;
	guidata(gcbo,myhandles);
	global L_msg;	
	global B_Report;
	global B_close;
	global globFrequencyId;
	global popup_freq;	
	
	if get(popup_channel,'Value') == 1 || get(popup_freq,'Value') == 1
		set(L_msg,'String','Select a Channel and Frequency');
	else	
		set(L_msg,'ForegroundColor',[0 .47 0]);
		set(L_msg,'String','Running...');
		set(hObject,'Enable','off');
		set(B_Reg,'Enable','off');
		set(B_Report,'Enable','off');
		set(B_close,'Enable','off');
		set(B_addChannel,'Enable','off');	
 		recProcThread(globFrequencyId);%external Function four
		set(L_msg,'ForegroundColor',[0 0 1]);
		set(L_msg,'String','No Input Signal');
		set(hObject,'Enable','on');
		set(B_Reg,'Enable','on');
		set(B_Report,'Enable','on');
		set(B_close,'Enable','on');
		set(B_addChannel,'Enable','on');
	end