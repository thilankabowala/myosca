function [Y,FS] = mp3read(FILE,N)

path = fileparts(which('mp3read'));

tmpdir = getenv('TMPDIR');
if isempty(tmpdir) || exist(tmpdir,'file')==0
  tmpdir = '/tmp';
end
if exist(tmpdir,'file')==0
  tmpdir = '';
end

rmcmd = 'rm';

ext = lower(computer);
if ispc
  ext = 'exe';
  rmcmd = 'del';
end

MPG123059 = 0;
mpg123 = fullfile(path,['mpg123.',ext]);
mp3info = fullfile(path,['mp3info.',ext]);

FMT = 'double';
N = [1 0];
forcemono = 0;
downsamp = 1;

%%%%% add extension if none (like wavread)
[path,file,ext] = fileparts(FILE);
if isempty(ext)
  FILE = [FILE, '.mp3'];
end


  %%%%%% Probe file to find format, size, etc. using "mp3info" utility
  cmd = ['"',mp3info, '" -r m -p "%Q %u %b %r %v * %C %e %E %L %O %o %p" "', FILE,'"'];
  % Q = samprate, u = #frames, b = #badframes (needed to get right answer from %u) 
  % r = bitrate, v = mpeg version (1/2/2.5)
  % C = Copyright, e = emph, E = CRC, L = layer, O = orig, o = mono, p = pad
  w = mysystem(cmd); 
  % Break into numerical and ascii parts by finding the delimiter we put in
  starpos = findstr(w,'*');
  nums = str2num(w(1:(starpos - 2)));
  strs = tokenize(w((starpos+2):end));

  SR = nums(1);
  nframes = nums(2);
  nchans = 2 - strcmp(strs{6}, 'mono');
  layer = length(strs{4});
  bitrate = nums(4)*1000;
  mpgv = nums(5);
  % Figure samples per frame, after
  % http://board.mp3-tech.org/view.php3?bn=agora_mp3techorg&key=1019510889
  if layer == 1
    smpspfrm = 384;
  elseif SR < 32000 && layer ==3
    smpspfrm = 576;
    if mpgv == 1
      error('SR < 32000 but mpeg version = 1');
    end
  else
    smpspfrm = 1152;
  end

  
if SR == 16000 && downsamp == 4
  error('mpg123 will not downsample 16 kHz files by 4 (only 2)');
end

delay = 0;

if downsamp == 1
  downsampstr = '';
else
  downsampstr = [' -',num2str(downsamp)];
end
FS = SR/downsamp;

if forcemono == 1
  nchans = 1;
  chansstr = ' -m';
else
  chansstr = '';
end

% Size-reading version
if strcmp(FMT,'size') == 1
   Y = [floor(smpspfrm*nframes/downsamp)-delay, nchans];
else
  
  tmpfile = fullfile(tmpdir, ['tmp',num2str(round(1000*rand(1))),'.wav']);	
  skipx = 0;
  skipblks = 0;
  skipstr = '';
  sttfrm = N(1)-1;

  if sttfrm > 0
    skipblks = floor(sttfrm*downsamp/smpspfrm);
    skipx = sttfrm - (skipblks*smpspfrm/downsamp);
    skipstr = [' -k ', num2str(skipblks)];
  end
  skipx = skipx + delay;
  
  lenstr = '';
  endfrm = -1;
  decblk = 0;
  if length(N) > 1
    endfrm = N(2);
    if endfrm > sttfrm
      decblk = ceil((endfrm+delay)*downsamp/smpspfrm) - skipblks + 10;   
      % we read 10 extra blks (+10) to cover the case where up to 10 bad 
      % blocks are included in the part we are trying to read (it happened)
      lenstr = [' -n ', num2str(decblk)];
      % This generates a spurious "Warn: requested..." if reading right 
      % to the last sample by index (or bad blks), but no matter.
    end
 end

  % Run the decode
  cmd=['"',mpg123,'"', downsampstr, chansstr, skipstr, lenstr, ...
       ' -q -w "', tmpfile,'"  "',FILE,'"'];
  %w = 
    mysystem(cmd);

  % Load the data

  Y = wavread(tmpfile);

  
%sound(Y,FS);
%  % pad delay on to end, just in case
%  Y = [Y; zeros(delay,size(Y,2))];
%  % no, the saved file is just longer
  
  if decblk > 0 && length(Y) < decblk*smpspfrm/downsamp
    % This will happen if the selected block range includes >1 bad block
    disp(['Warn: requested ', num2str(decblk*smpspfrm/downsamp),' frames, returned ',num2str(length(Y))]);
  end
  
  % Delete tmp file
 mysystem([rmcmd,' "', tmpfile,'"']);
  
  % debug
%  disp(['sttfrm=',num2str(sttfrm),' endfrm=',num2str(endfrm),' skipx=',num2str(skipx),' delay=',num2str(delay),' len=',num2str(length(Y))]);
  
  % Select the desired part
  if skipx+endfrm-sttfrm > length(Y)
      endfrm = length(Y)+sttfrm-skipx;
  end
  
  if endfrm > sttfrm
    Y = Y(skipx+(1:(endfrm-sttfrm)),:);
  elseif skipx > 0
    Y = Y((skipx+1):end,:);
  end
  
  % Convert to int if format = 'native'
  if strcmp(FMT,'native')
    Y = int16((2^15)*Y);
  end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function w = mysystem(cmd)
% Run system command; report error; strip all but last line
[s,w] = system(cmd);
if s ~= 0 
  error(['unable to execute ',cmd,' (',w,')']);
end
% Keep just final line
w = w((1+max([0,findstr(w,10)])):end);
% Debug
%disp([cmd,' -> ','*',w,'*']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function a = tokenize(s,t)
% Break space-separated string into cell array of strings.
% Optional second arg gives alternate separator (default ' ')
% 2004-09-18 dpwe@ee.columbia.edu
if nargin < 2;  t = ' '; end
a = [];
p = 1;
n = 1;
l = length(s);
nss = findstr([s(p:end),t],t);
for ns = nss
  % Skip initial spaces (separators)
  if ns == p
    p = p+1;
  else
    if p <= l
      a{n} = s(p:(ns-1));
      n = n+1;
      p = ns+1;
    end
  end
end
    
