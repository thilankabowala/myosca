classdef myLL

properties
	myHead
	myTail
end

methods
	function obj = myLL()
		myHead=dlnode();
		myTail=dlnode();			
	end

	function [obj,node] = addToHead(myObj,currantHead,myArg)	
			if nargin == 3
				n1=dlnode(myArg);
			else
				n1=dlnode();
			end
		if(length(myObj.myHead) == 0)		
			myObj.myHead = n1;
		else
			n1.insertAfter(currantHead);
		end		
		
		obj = myObj;
		node = n1;
	end

	function retHead = getHead(myObj)		
		retHead = myObj.myHead;
	end
end

end