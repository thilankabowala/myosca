function testing(path)
	if isdir(path)	
		%fileName = regexp(path,'\','split');	
		allFiles = dir(path);
		for processFile = 1 : length(allFiles)
			if strcmp(allFiles(processFile).name,'.')==0 && strcmp(allFiles(processFile).name,'..') == 0			
				if isdir([path '\' allFiles(processFile).name])
					addDirectory([path '\' allFiles(processFile).name]);
				else					
					title = regexp(allFiles(processFile).name,'\.','split');	
					if strcmp(char(title(2)),'wav') || strcmp(char(title(2)),'WAV')						
						exeTest([path,'\',allFiles(processFile).name],char(title(1)),0);							
					end						
				end
			end		
		end
	else	
		[path,name,extent] = fileparts(path);
		title = regexp(path,'\.','split');	
		if strcmp(char(extent),'.wav') || strcmp(char(extent),'.WAV')						
			exeTest([path '\' name],char(title(1)),0);							
		end
	end


function exeTest(path,actualTitle,verbose,note)
	[Data,SR] = wavread(path);
	R = patternMatch(Data,SR); 
	matchedPercent = 13;
	if nargin < 4		
		note = 'Undefined';
	end
	if verbose == 1
		detected_song = R(R(:,3) > 5,:);
		detected_song
	else
		if(size(R,1) ~= 0) && (length(R(R(:,3) > matchedPercent,:)) ~= 0)	
			connection = database_connect();	
			detected_song = R(R(:,3) > matchedPercent,:);
			recog = 1;
				%for recog = 1:length(detected_song(:,1))
					Query = ['select title,singer from meta_data where meta_data.song_id = ' num2str(detected_song(recog,1))];
					[getHash]=fetch(connection,Query);
					myId = fopen('testData.txt', 'a+');
					fwrite(myId,['actual song -> ' actualTitle  '  matched song ->' char(getHash(1)) ' by ' char(getHash(2)) '  Note: ' note]);
					fprintf(myId,'\n');
					fclose(myId);
				%end
		end
	end