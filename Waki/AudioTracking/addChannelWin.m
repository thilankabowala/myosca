function addChannelWin()
%x y w h ,'resize','of'
global addChannelWindow;
global ET_Channel;
global ET_Freq;
global L_loading;
addChannelWindow = figure('Color',[1 1 1],'Position',[480 300 400 180]);

	mainTitle = uicontrol('Style','text','String','Add Radio Channel','ForegroundColor',[.54 .59 .99],'backg',[1 1 1],'pos',[50 120 300 50],'fonts',16,'FontWeight','bold');

	L_title = uicontrol('Style','text','String','Channel Name','backg',[1 1 1],'pos',[20 105 100 20],'fonts',10,'HorizontalAlignment','left');
	ET_Channel = uicontrol('Style','edit','backg',[1 1 1],'pos',[160 105 200 20],'fonts',10,'HorizontalAlignment','left');
	
	L_title = uicontrol('Style','text','String','Channel Frequencies','backg',[1 1 1],'pos',[20 80 170 20],'fonts',10,'HorizontalAlignment','left');
	ET_Freq = uicontrol('Style','edit','backg',[1 1 1],'pos',[160 80 200 20],'fonts',10,'HorizontalAlignment','left');
	
	B_add = uicontrol('Style','pushbutton','String','Add Channel','Callback',@getData,'pos',[100 28 100 30]);
	B_cancel = uicontrol('Style','pushbutton','String','Cancel','Callback',@closeWin,'pos',[220 28 100 30]);
	
	L_loading = uicontrol('Style','text','backg',[1 1 1],'pos',[0 4 400 20],'fonts',9,'ForegroundColor',[1 0 0],'FontWeight','bold');
	
function closeWin(hObject,eventdata)
	global addChannelWindow;
	close(addChannelWindow);
	homeWin();
	
function getData(hObject,eventdata)		
	global ET_Channel;
	global ET_Freq;
	global L_loading;
	set(L_loading,'String','');	
	channel_name = get(ET_Channel,'String');
	frequency_set = get(ET_Freq,'String');
	parts = regexp(frequency_set,',','split');
	isvalid = 1;
	if length(str2num(char(parts(1)))) ~= 0	
		for i=1:length(parts)
			if length(str2num(char(parts(i)))) == 0
				set(L_loading,'String','Invalid Frequency Set');	
				isvalid = 0;
				break;
			end
		end
	else
		set(L_loading,'String','Frequency Field is Required !');
		isvalid = 0;
	end
	
	if isvalid==1
		isSuccess = addChannel(channel_name,parts);
		if isSuccess == 0
			set(L_loading,'String','Channel Name Already Exist !');	
		else
			set(L_loading,'ForegroundColor',[0 .4 0]);	
			set(L_loading,'String','Channel Added Successfully !');	
		end
	end