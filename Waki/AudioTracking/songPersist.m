function songPersist(H)
%processed
	nhash = size(H,1);
	TIMESIZE = 16384;

	connection = database_connect();
	QUERY1 = ['insert ignore into hash_lookup(hash_id) values '];
	QUERY2 = ['insert into hitted_song values '];
	QUERY3 = ['update hash_lookup set num_hits = num_hits+1 where '];

	for i=1:nhash-1
		song = H(i,1);
		toffs = mod(round(H(i,2)), TIMESIZE);
		hash = 1+H(i,3);  % avoid problems with hash == 0  
		hashval = int32(song*TIMESIZE + toffs);

		QUERY1 = [QUERY1 '(' num2str(hash) '),'];
		QUERY2 = [QUERY2 '(' num2str(hash) ',''' num2str(hashval) ''',' num2str(song) '),'];
		QUERY3 = [QUERY3 'hash_id = ' num2str(hash) ' or ' ];   
	end

	i = nhash;
	song = H(i,1);
	toffs = mod(round(H(i,2)), TIMESIZE);
	hash = 1+H(i,3);
	hashval = int32(song*TIMESIZE + toffs);
	QUERY1 = [QUERY1 '(' num2str(hash) ')'];
	QUERY2 = [QUERY2 '(' num2str(hash) ',''' num2str(hashval) ''',' num2str(song) ')'];
	QUERY3 = [QUERY3 'hash_id = ' num2str(hash) ];

	exec(connection,QUERY1);	
	exec(connection,QUERY2);	
	exec(connection,QUERY3);	
	close(connection);