function seperate(path)
	if isdir(path)	
		allFiles = dir(path);
		for processFile = 1 : length(allFiles)
			if strcmp(allFiles(processFile).name,'.')==0 && strcmp(allFiles(processFile).name,'..') == 0			
				if isdir([path '\' allFiles(processFile).name])
					seperate([path '\' allFiles(processFile).name]);
				else					
					title = regexp(allFiles(processFile).name,'\.','split');	
					if strcmp(char(title(2)),'wav') || strcmp(char(title(2)),'WAV')		
						disp(['processing : ' path,'\',allFiles(processFile).name]);
						matchEach([path,'\',allFiles(processFile).name],1);	
					elseif strcmp(char(title(2)),'mp3') || strcmp(char(title(2)),'MP3')
						disp(['processing : ' path,'\',allFiles(processFile).name]);
						matchEach([path,'\',allFiles(processFile).name],0);							
					end						
				end
			end		
		end
	else	
		[path,name,extent] = fileparts(path);
		title = regexp(path,'\.','split');	
		if strcmp(char(extent),'.wav') || strcmp(char(extent),'.WAV')	
			disp(['processing : ' path '\' name]);
			matchEach([path '\' name],1);		
		elseif strcmp(char(extent),'.mp3') || strcmp(char(extent),'.MP3')
			disp(['processing : ' path '\' name]);
			matchEach([path '\' name],0);	
		end
	end


function matchEach(path,type)

	if type == 1
		[Data,SR] = wavread(path);
	else
		[Data,SR] = mp3read(path);
	end

	framed = frameing(Data,SR); %external function one processed
	myHead = framed;	
	numFr = 1;
	myId = fopen('tempTest.txt', 'a+');
	while(length(myHead.Data) ~= 0)
		disp(['Processing frame ' num2str(numFr)]);
		matchedPercent = 13;
		originalFrame = mean(myHead.Data,2);
		originalFrame(originalFrame < 0)=0;
		clipSilences = numSilences(myHead.Data,SR,mean(originalFrame)+0.001);
		maxClipSilences = 50;
		
		if clipSilences <= maxClipSilences						
			fwrite(myId,['detectedSongClips ' num2str(clipSilences)]);
			fprintf(myId,'\n');			
			%myWaveWrite(myHead.Data,SR,'detectedSongClips');
		else						
			fwrite(myId,['nonSongObjects ' num2str(clipSilences)]);
			fprintf(myId,'\n');			
			%myWaveWrite(myHead.Data,SR,'nonSongObjects'); %external function five processed
		
		end		
		myHead = myHead.Next;
		myHead.Prev.delete;
		numFr = numFr + 1;
	end	
	fclose(myId);