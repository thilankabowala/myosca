
function conn = database_connect()

host = 'localhost:3306';
user = 'root';
password = '';
dbName = 'osca';%change name to osca from audio_track - by thippi
jdbcString = sprintf('jdbc:mysql://%s/%s', host, dbName);
jdbcDriver = 'com.mysql.jdbc.Driver';
dbConn = database(dbName, user, password, jdbcDriver, jdbcString);
conn = dbConn;