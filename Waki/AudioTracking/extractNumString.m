function extractNumString()
	myId = fopen('toProcess.txt');
	%A = fscanf(myId, '%s');
	tline = fgets(myId)
	count = 0;
	countSong = 0;
	while (tline ~= -1)
		if length(tline) > 15 && strcmp(tline(1:14),'nonSongObjects') == 1
			count = count + 1;			
		elseif length(tline) > 18 && strcmp(tline(1:17),'detectedSongClips') == 1
			countSong = countSong+1;
		end
		tline = fgets(myId);
	end
	fclose(myId);
	disp(['Number of non-song object : ' num2str(count) ]);
	disp(['Number of song object : ' num2str(countSong) ]);
