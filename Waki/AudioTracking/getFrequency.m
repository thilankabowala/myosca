function frequencies = getFrequency(channelName)
	connection = database_connect();		
	QUERY = ['select frequency,frequency_id from channels,channel_frequencies where channels.channel_id=channel_frequencies.channel_id and channels.channel_name = ''' channelName ''''];	
	[freqs]=fetch(connection,QUERY);	
	frequencies = freqs;
	close(connection);