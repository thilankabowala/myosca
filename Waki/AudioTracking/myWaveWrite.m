function myWaveWrite(dataAry,SR,filePath,timeStamp)
	currentT = datestr(now,'yyyy-mm-dd HH-MM-SS');
    if nargin == 4
        currentT = [currentT '_' num2str(timeStamp)];
    end
	wavwrite(dataAry,SR,[filePath '/' num2str(currentT) '.wav']);


