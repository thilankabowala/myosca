function genOk = generateReport()
%processed
	genOk = 0;		
	DataAry = {'Song Title','Singer','Musician','author','Start Time','End Time','Id'};		
	connection = database_connect();
	QUERY2 = ['select title,singer,musician,author,begin_time,end_time,report_data.detected_id from report_data,meta_data where meta_data.song_id = report_data.song_id order by report_data.detected_id'];
	[Mydata]=fetch(connection,QUERY2);
		
	if length(Mydata) ~= 0
		DataAry = cat(1,DataAry,Mydata);
		assignin('base', 'ReportData', DataAry);
		QUERY = ['select channels.channel_name,channel_frequencies.frequency from channels,channel_frequencies where channel_frequencies.channel_id=channels.channel_id and channel_frequencies.frequency_id = (select frequency_id from report_data  order by  frequency_id desc limit 1 )'];
		[channelData]=fetch(connection,QUERY);		
		assignin('base', 'Channel', channelData);
		report generatePDF -fpdf
		clear ReportData;
		genOk = 1;
	end
	close(connection);