<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<!-- PDF generator javascript files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.from_html.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.split_text_to_size.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.standard_fonts_metrics.js') ?>"></script>
	
</head>


<body>
<div class="container">

    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Report</h3>
			
			
			<form>
			<table>
				
				<?php
					$reportNumber = 0;
					foreach ($records as $key ) {
						$reportNumber = $reportNumber + 1;
						echo "<tr><th>Report ".$reportNumber."</th></tr>".
								"<tr><td>Broadcasting ID: ".$key->detected_id."</td></tr>".
								"<tr><td>Begin Time: ".$key->begin_time."</td></tr>".
								"<tr><td>End Time: ".$key->end_time."</td></tr>".
								"<tr><td>Channel ID: ".$key->channel_id."</td></tr>".
								"<tr><td>Channel Name: ".$key->channelName."</td></tr>".
								"<tr><td>Frequency ID: ".$key->frequency_id	."</td></tr>".
								"<tr><td>Frequency: ".$key->frequency."</td></tr>".
								"<tr><td>Song ID: ".$key->song_id."</td></tr>".
								"<tr><td>Song Name: ".$key->songName."</td></tr>".
								"<tr><td>Score: ".$key->score."</td></tr>".
								"<tr><td>Artist ID: ".$key->artist."</td></tr>".
								"<tr><td>Composer ID: ".$key->composer."</td></tr>".
								"<tr><td>Lyricist ID: ".$key->lyricist."</td></tr>";
					}
					if($reportNumber = 1){
						//echo "No data to view";
					}
				?>
			</table>
			
			</form>

        </article>
		
	<script>
		var doc = new jsPDF();          
		var elementHandler = {
		  '#ignorePDF': function (element, renderer) {
			return true;
		  }
		};
		var source = window.document.getElementsByTagName("table")[0];
		doc.fromHTML(
			source,
			15,
			15,
			{
			  'width': 180,'elementHandlers': elementHandler
			});

		doc.output("dataurlnewwindow");
	</script>
