</br>
</br>
</br>
</br>
</br>
</br>
<aside class="col-lg-2 col-lg-pull-10">

    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
			<li class='<?=($this->uri->segment(2)=="addArtist")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/addArtist') ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Artist</a></li>
            <li class='<?=($this->uri->segment(2)=="addComposer")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/addComposer') ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Composer</a></li>
            <li class='<?=($this->uri->segment(2)=="addLyricist")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/addLyricist') ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Lyricist</a></li>
        </ul>
    </div>
    </br>
    </br>
    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
			<li class='<?=($this->uri->segment(2)=="viewArtist")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/viewArtist') ?>"><span class="glyphicon  glyphicon-user"></span>&nbsp;&nbsp;View Artists</a></li>
            <li class='<?=($this->uri->segment(2)=="viewComposer")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/viewComposer') ?>"><span class="glyphicon  glyphicon-music"></span>&nbsp;&nbsp;View Composers</a></li>
            <li class='<?=($this->uri->segment(2)=="viewLyricist")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists/viewLyricist') ?>"><span class="glyphicon  glyphicon-pencil"></span>&nbsp;&nbsp;View Lyricists</a></li>
        </ul>
    </div>
	
</aside>


</div>




</div>
</body>

</html>