</br>
</br>
</br>
</br>
</br>
</br>
<aside class="col-lg-2 col-lg-pull-10">

    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
            <li class='<?=($this->uri->segment(2)=="addChannel")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/addChannel') ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Channel</a></li>
            <li class='<?=($this->uri->segment(2)=="editChannel")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/editChannel') ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;Edit Channel</a></li>
            <li class='<?=($this->uri->segment(2)=="deleteChannel")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/deleteChannel') ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Delete Channel</a></li>
        </ul>
    </div>
    </br>
    </br>
    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
			<li class='<?=($this->uri->segment(2)=="viewChannel")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/viewChannel') ?>"><span class="glyphicon glyphicon-sound-5-1"></span>&nbsp;&nbsp;View Channels</a></li>
			<li class='<?=($this->uri->segment(2)=="selectChannelToView")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/selectChannelToView') ?>"><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Specific Channel</a></li>
			<li class='<?=($this->uri->segment(2)=="selectFrequencyToView")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels/selectFrequencyToView') ?>"><span class="glyphicon glyphicon-sound-7-1"></span>&nbsp;&nbsp;By Frequency</a></li>
        </ul>
    </div>
	
</aside>


</div>




</div>
</body>

</html>