<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Songs</h3>
			
			<br>
			
			<table class="table table-bordered">
				<tr>
					<th>Song ID</th>
					<th>Song Name</th>
					<th>Matadata ID</th>
					<th>Artist ID</th>
					<th>Artist Name</th>
					<th>Composer ID</th>
					<th>Composer Name</th>
					<th>Lyricist ID</th>
					<th>Lyricist Name</th>
				</tr>
				<?php
					foreach ($records as $key ) {
						echo "<tr>"."<td>".$key->idsong."</td>".
								"<td>".$key->songName."</td>".
								"<td>".$key->idmatadata."</td>".
								"<td>".$key->artist."</td>".
								"<td>".$key->afirstName." ".$key->amiddlName." ".$key->alastName."</td>".
								"<td>".$key->composer."</td>".
								"<td>".$key->cfirstName." ".$key->cmiddlName." ".$key->clastName."</td>".
								"<td>".$key->lyricist."</td>".
								"<td>".$key->lfirstName." ".$key->lmiddlName." ".$key->llastName."</td>".
								"</tr>";
					}
				?>
			</table>


        </article>

