<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
</head>


<body>

<div class="container">
    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Delete Song</h3>
            <div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
                <form class="addition" role="form" action="<?php echo base_url();?>index.php/song/deleteSelectedSong" method="POST">

                        <label>Select Song</label>
                        </br>
                        <select class="form-control" name="song_name" required>
                            <option disabled selected >-----Song list-----</option>
                            <?php
                                foreach ($records as $key ) {
                                 echo "<option>".$key->songName."</option>";
                                }
                            ?>

                        </select>
						<button type="submit" class="btn btn-primary">Delete Song</button>

				</form>
            </div>
        </article>

