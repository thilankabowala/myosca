</br>
</br>
</br>
</br>
</br>
</br>
<aside class="col-lg-2 col-lg-pull-10">

    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
            <li class='<?=($this->uri->segment(2)=="add")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/add') ?>"><span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Add Song</a></li>
            <li class='<?=($this->uri->segment(2)=="selectSongToEdit")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectSongToEdit') ?>"><span class="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;Edit Song</a></li>
            <li class='<?=($this->uri->segment(2)=="selectSongToDelete")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectSongToDelete') ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;&nbsp;Delete Song</a></li>
        </ul>
    </div>
    </br>
    </br>
    <div class="sidebar-nav">
        <ul class="nav nav-pills nav-stacked" role="tablist">
            <li class='<?=($this->uri->segment(2)=="viewSong")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/viewSong') ?>"><span class="glyphicon  glyphicon-headphones"></span>&nbsp;&nbsp;View Songs</a></li>
            <li class='<?=($this->uri->segment(2)=="selectSongToView")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectSongToView') ?>"><span class="glyphicon  glyphicon-filter"></span>&nbsp;&nbsp;Specific Song</a></li>
            <li class='<?=($this->uri->segment(2)=="selectArtistToViewSong")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectArtistToViewSong') ?>"><span class="glyphicon  glyphicon-user"></span>&nbsp;&nbsp;By Artist</a></li>
            <li class='<?=($this->uri->segment(2)=="selectComposerToViewSong")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectComposerToViewSong') ?>"><span class="glyphicon  glyphicon-music"></span>&nbsp;&nbsp;By Composer</a></li>
            <li class='<?=($this->uri->segment(2)=="selectLyricistToViewSong")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song/selectLyricistToViewSong') ?>"><span class="glyphicon  glyphicon-pencil"></span>&nbsp;&nbsp;By Lyricist</a></li>
        </ul>
    </div>
	
</aside>


</div>




</div>
</body>

</html>