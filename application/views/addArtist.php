<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
    <script type="text/javascript">

        function formValidator() {

            var id = document.getElementById('artist_id');
            var firstname = document.getElementById('artist_fname');
            var middlename = document.getElementById('artist_mname');
            var lastname = document.getElementById('artist_lname');


            if (idCheck(id, "please start with letter a and then a number")) {
                if (lengthRestriction(id, 2, 5)) {
                    if (isAlphabet(firstname, "Please enter only letters for the name")) {
                        if (startWithCap(firstname, "Please start the name with a capital letter")) {
                            if (mid(middlename, "Please enter only letters for the name")) {
                                if (startWithCap(middlename, "Please start the name with a capital letter")) {
                                    if (isAlphabet(lastname, "Please enter only letters for the name")) {
                                        if (startWithCap(lastname, "Please start the name with a capital letter")) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;

        }

        function isAlphabet(elem, helperMsg){
            var alphaExp = /^[a-zA-Z]+$/;
            if(elem.value.match(alphaExp)){
                return true;
            }else{
                alert(helperMsg);
                elem.focus();
                return false;
            }
        }

        function mid(elem, helperMsg){
            var alphaExp = /^[a-zA-Z]+$/;
            if(elem.value==""){
                return true;
            }
            else if(elem.value.match(alphaExp)){
                return true;
            }else{
                alert(helperMsg);
                elem.focus();
                return false;
            }
        }

        function startWithCap(elem, helperMsg){
            var alphaExp = /[A-Z]/;
            if(elem.value==""){
                return true;
            }
            else if(elem.value.charAt(0).match(alphaExp)){
                return true;
            }else{
                alert(helperMsg);
                elem.focus();
                return false;
            }
        }

        function idCheck(elem, helperMsg){
            var alphaExp = /^[a][0-9]+$/;
            if( elem.value.match(alphaExp)){
                return true;
            }else{
                alert(helperMsg);
                elem.focus();
                return false;
            }
        }

        function lengthRestriction(elem, min, max){
            var uInput = elem.value;
            if(uInput.length >= min && uInput.length <= max){
                return true;
            }else{
                alert("Please enter between " +min+ " and " +max+ " characters");
                elem.focus();
                return false;
            }
        }

    </script>
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Add Artist</h3>
            <div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
            <form class="addition" role="form" onsubmit="return formValidator()" action="<?php echo base_url();?>index.php/artists/insertArtist" method="POST">

                <div class="form-group ">
                    <label>Artist  ID</label>
                    </br>
                    <input type="text" class="form-control" name="artist_id" id="artist_id" placeholder="Artist ID" required>

                </div>

                <div class="form-group ">
                    <label>First Name</label>
                    </br>
                    <input type="text" class="form-control" name="artist_fname" id="artist_fname" placeholder="Artist First Name" required>
                </div>

                <div class="form-group ">
                    <label>Middle Name</label>
                    </br>
                    <input type="text" class="form-control" name="artist_mname" id="artist_mname" placeholder="Artist Middle Name" required>
                </div>

                <div class="form-group ">
                    <label>Last Name</label>
                    </br>
                    <input type="text" class="form-control" name="artist_lname" id="artist_lname" placeholder="Artist Last Name" required>
                </div>

                <div class="form-group ">
                    <label>Residence Address</label>

                </div>

                <div class="form-group ">
                    <label>House No:</label>
                    </br>
                    <input type="text" class="form-control" name="artist_hnum" placeholder="House number / name" required>
                </div>

                <div class="form-group ">
                    <label>Street</label>
                    </br>
                    <input type="text" class="form-control" name="artist_street" placeholder="Street" required>
                </div>

                <div class="form-group ">
                    <label>City</label>
                    </br>
                    <input type="text" class="form-control" name="artist_city" placeholder="City" required>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>
        </article>

