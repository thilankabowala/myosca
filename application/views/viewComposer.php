<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Composers</h3>
			<br>
			
			<table class="table table-bordered">
            <tr>
                <th>Composer ID</th>
                <th>First Name</th>
                <th>Middle Name</th>
				<th>Last Name</th>
				<th>Number of the House</th>
				<th>Street</th>
				<th>City</th>
            </tr>
            <?php
                foreach ($records as $key ) {
                    echo "<tr>"."<td>".$key->idComposer."</td>".
                            "<td>".$key->cfirstName."</td>".
                            "<td>".$key->cmiddlName."</td>".
                            "<td>".$key->clastName."</td>".
							"<td>".$key->houseNumber."</td>".
							"<td>".$key->street."</td>".
							"<td>".$key->city."</td>".
                            "</tr>";
                }
            ?>
			</table>


        </article>

