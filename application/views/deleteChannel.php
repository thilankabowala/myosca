<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<script>
		function validateForm() {
			var x = document.forms["myForm"]["channel_name"].value;
			if (x == null || x == "") {
				alert("Channel name must be filled out");
				return false;
			}
		}
	</script>
	
	
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Delete Channel</h3>
            <div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
                <form class="addition" role="form" action="<?php echo base_url();?>index.php/channels/deleteSelectedChannel" method="POST">

                    <div class="form-group add_more_artist">
                        <label>Select Channel</label>
                        </br>
                        <select class="form-control" name="channel_name" required>
                            <option disabled selected >-----Channel list-----</option>
                            <?php
                                foreach ($records as $key ) {
                                 echo "<option>".$key->channelName."</option>";
                                }
                            ?>

                        </select>
						<button type="submit" class="btn btn-primary">Delete Channel</button>

                    </div>
				</form>
            </div>
        </article>

