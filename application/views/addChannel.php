<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<script>
		function validateForm() {
			var x = document.forms["myForm"]["channel_name"].value;
			if (x == null || x == "") {
				alert("Channel name must be filled out");
				return false;
			}
		}
	</script>
	
	<!-- Script for add more Frequency-->
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 3; //maximum input boxes allowed
            var wrapper         = $(".add_more_frequency"); //Fields wrapper
            var add_button      = $(".add_frequency_button"); //Add button ID
            text1 = '<div><input type="text" class="form-control" name="channel_frequency" placeholder="Frequency" required'
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append(text1+x+'"><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }else{alert("Can't add more than 3 frequencies");}
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });</script>
	
	
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Add Channel</h3>
            <div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
                <form class="addition" role="form" name="myForm" action="<?php echo base_url();?>index.php/channels/insertChannel" method="POST" onsubmit="return validateForm()">

                    <div class="form-group ">
                        <label>Channel ID</label>
                        </br>
                        <input type="text" class="form-control" name="channel_id" placeholder="Channel ID">
                    </div>

                    <div class="form-group ">
                        <label>Channel Name</label>
                        </br>
                        <input type="text" class="form-control" name="channel_name" placeholder="Channel Name" required>
						
                    </div>
					
					<div class="form-group add_more_frequency">
                        <label>Channel Frequency</label>
                        </br>
                        <input type="text" class="form-control" name="channel_frequency" placeholder="Frequency" required>
						<!--textarea name="channel_frequency" rows="3" cols="25"> </textarea-->
						<button class="btn btn-default add_frequency_button">+</button>
                    </div>
					

                    <div class="form-group ">
                        <label>Channel Address</label>
                    </div>

                    <div class="form-group ">
                        <label>Address No:</label>
                        </br>
                        <input type="text" class="form-control" name="address_num" placeholder="Address number / name" required>
                    </div>

                    <div class="form-group ">
                        <label>Street</label>
                        </br>
                        <input type="text" class="form-control" name="address_street" placeholder="Street">
                    </div>

                    <div class="form-group ">
                        <label>City</label>
                        </br>
                        <input type="text" class="form-control" name="address_city" placeholder="City" required>
                    </div>
					
					<div class="form-group ">
                        <label>Postal Code</label>
                        </br>
                        <input type="text" class="form-control" name="address_postal_code" placeholder="Postal Code" required>
                    </div>
					
                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>
            </div>
        </article>

