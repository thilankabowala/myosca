<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<!-- PDF generator javascript files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.from_html.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.split_text_to_size.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.standard_fonts_metrics.js') ?>"></script>
	
	<!-- Date Picker javascript files -->
	<!-- Load jQuery from Google's CDN -->
    <!-- Load jQuery UI CSS  -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Load SCRIPT.JS which will create datepicker for input field  -->
    <script src="<?php echo base_url('assets/js/script.js') ?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/runnable.css') ?>" />
	
</head>


<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Report</h3>
			
			<br>
			
			<p>Pick a Date: <input type="text" id="datepicker" /></p>
			
			<form>
			<table class="table table-bordered">
				<tr>
					<th>Broadcasting ID</th>
					<th>Broadcasting Date</th>
					<th>Broadcasting Time</th>
					<th>Channel ID</th>
					<th>Channel Name</th>
					<th>Broadcasting Part File Name</th>
					<th>Broadcasting Part File Path</th>
					<th>Song ID</th>
					<th>Song Name</th>
					<th>Artist ID</th>
					<th>Composer ID</th>
					<th>Lyricist ID</th>
					<th>Mp3 File ID</th>
					<th>Mp3 File Name</th>
					<th>Mp3 File Path</th>
				</tr>
				<?php
					foreach ($records as $key ) {
						echo "<tr>"."<td>".$key->broadcastingID."</td>".
								"<td>".$key->date."</td>".
								"<td>".$key->time."</td>".
								"<td>".$key->channelID."</td>".
								"<td>".$key->channelName."</td>".
								"<td>".$key->broadcastingPartFileName."</td>".
								"<td>".$key->broadcastingPartFilePath."</td>".
								"<td>".$key->songID."</td>".
								"<td>".$key->code."</td>".
								"<td>".$key->artist."</td>".
								"<td>".$key->composer."</td>".
								"<td>".$key->lyricist."</td>".
								"<td>".$key->mp3file."</td>".
								"<td>".$key->fileName."</td>".
								"<td>".$key->filePath."</td>".
								"</tr>";
					}
				?>
			</table>
			</form>
			
			<!--button type="submit" class="btn btn-primary">Generate PDF Report</button-->


        </article>
<script>
		var doc = new jsPDF();          
		var elementHandler = {
		  '#ignorePDF': function (element, renderer) {
			return true;
		  }
		};
		var source = window.document.getElementsByTagName("table")[0];
		doc.fromHTML(
			source,
			15,
			15,
			{
			  'width': 180,'elementHandlers': elementHandler
			});

		doc.output("dataurlnewwindow");
	</script>
