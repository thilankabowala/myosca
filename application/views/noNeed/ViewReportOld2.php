<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<!-- PDF generator javascript files -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.from_html.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.split_text_to_size.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jspdf.plugin.standard_fonts_metrics.js') ?>"></script>
	
	<!-- Date Picker javascript files -->
	<!-- Load jQuery from Google's CDN -->
    <!-- Load jQuery UI CSS  -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <!-- Load jQuery JS -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- Load SCRIPT.JS which will create datepicker for input field  -->
    <script src="<?php echo base_url('assets/js/script.js') ?>"></script>
    <!--link rel="stylesheet" href="<?php //echo base_url('assets/css/runnable.css') ?>" /-->
	
</head>


<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Report</h3>
			
			<br>
			<p>Pick a Date: <input type="text" id="datepicker" /></p>
			
			<form>
			<table>
				
				<?php
					foreach ($records as $key ) {
						echo "<tr><th>New Report </th></tr>".
								"<tr><td>Broadcasting ID: ".$key->broadcastingID."</td></tr>".
								"<tr><td>Broadcasting Date: ".$key->date."</td></tr>".
								"<tr><td>Broadcasting Time: ".$key->time."</td></tr>".
								"<tr><td>Channel ID: ".$key->channelID."</td></tr>".
								"<tr><td>Channel Name: ".$key->channelName."</td></tr>".
								"<tr><td>Broadcasting Part File Name: ".$key->broadcastingPartFileName."</td></tr>".
								"<tr><td>Broadcasting Part File Path: ".$key->broadcastingPartFilePath."</td></tr>".
								"<tr><td>Song ID: ".$key->songID."</td></tr>".
								"<tr><td>Song Name: ".$key->code."</td></tr>".
								"<tr><td>Artist ID: ".$key->artist."</td></tr>".
								"<tr><td>Composer ID: ".$key->composer."</td></tr>".
								"<tr><td>Lyricist ID: ".$key->lyricist."</td></tr>".
								"<tr><td>Mp3 File ID: ".$key->mp3file."</td></tr>".
								"<tr><td>Mp3 File Name: ".$key->fileName."</td></tr>".
								"<tr><td>Mp3 File Path: ".$key->filePath."</td></tr>";
					}
				?>
			</table>
			
			</form>
			
			<!--button type="submit" class="btn btn-primary">Generate PDF Report</button-->


        </article>
		
	<script>
		var doc = new jsPDF();          
		var elementHandler = {
		  '#ignorePDF': function (element, renderer) {
			return true;
		  }
		};
		var source = window.document.getElementsByTagName("table")[0];
		doc.fromHTML(
			source,
			15,
			15,
			{
			  'width': 180,'elementHandlers': elementHandler
			});

		doc.output("dataurlnewwindow");
	</script>
