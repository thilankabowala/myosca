<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/reset.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/structure.css') ?>" rel="stylesheet">
</head>
<header id="osca">
    <p>OSCA</p>
</header>
<header id="ams">
    <p>Audio Monitoring System</p>
</header>
<body>

<form class="box login" method="post" action="checklogin.php">
    <fieldset class="boxBody">
        <label>Username</label>
        <input name="myusername" type="text" id="myusername" required>
        <label><a href="#" class="rLink" tabindex="5">Forget your password?</a>Password</label>
        <input name="mypassword" type="password" id="mypassword" required>
        <label><input type="checkbox" tabindex="3">Keep me logged in</label>
        <input type="submit" class="btnLogin" value="Login" tabindex="4">
    </fieldset>
    <footer>

    </footer>
</form>
<footer id="main">
    <p>University of Colombo School of Computing</p>
</footer>

</body>
</html>