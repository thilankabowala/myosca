<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
	
	<!--script type="text/javascript">
		function myFunction(x) {
			var allow = true;
			if(allow){
						allow = false;
						$("#result").html('loading...');
						$.ajax({
							url:'http://localhost/helpdesk?q='+escape($("#q").val()),
							success: function (data){
								$("#result").html(data);
								allow = true;
							}
						});
		}
	</script-->
	
</head>


<body>
<div class="container">

    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Report By Channel</h3>
			
			
			<div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
			
			
				<!--form class="addition" role="form" action="<?php echo base_url();?>index.php/Report/viewReport" method="POST">

					<table border="0" cellpadding="0" cellspacing="2" width="100%">
						<tbody>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;Select Channel : </td>
								<td align="left" valign="middle">
									<input class="ac_input" style="" name="lccp_stncode_dis" id="txtStation" onfocus="myFunction(this)" size="50" value="" maxlength="50" autocomplete="off" type="text">
									<input name="lccp_stncode" value="" type="hidden">
								</td>   
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><input name="getIt" value="Generate PDF Report" onclick="if(submitting) { alert('Enquiry in progress, please wait ...'); this.disabled = true; return false; } else if(validateform(this.form)) { this.value = 'Please Wait...'; submitting = true; return true; } else { return false; } " class="btn btn-primary" type="submit">
									<input name="clearData" value="Clear" class="btn btn-primary" type="reset"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
                    
				</form-->
			
			
			
			
                <form class="addition" role="form" action="<?php echo base_url();?>index.php/Report/viewReportByChannel" method="POST">

                    <div class="form-group add_more_artist">
                        <label>Select Channel</label>
                        </br>
                        <select class="form-control" name="channel_name" required>
                            <option disabled selected >-----Channel list-----</option>
                            <?php
                                foreach ($records as $key ) {
                                 echo "<option>".$key->channelName."</option>";
                                }
                            ?>

                        </select>
						<button type="submit" class="btn btn-primary">Generate PDF Report</button>

                    </div>
				</form>
			

        </article>
		
	