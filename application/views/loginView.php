<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/reset.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/structure.css') ?>" rel="stylesheet">
</head>
<header id="osca">
    <p>OSCA</p>
</header>
<header id="ams">
    <p>Audio Monitoring System</p>
</header>
<body>


<?php echo form_open('verifylogin'); ?>
<div class="box login">
    <fieldset class="boxBody">
        <label>Username</label>
        <input name="username" type="text" id="username" required>
        <label><a href="#" class="rLink" tabindex="5">Forget your password?</a>Password</label>
        <input name="password" type="password" id="password" required>
        <label><input type="checkbox" tabindex="3">Keep me logged in</label>
        <input type="submit" class="btnLogin" value="Login" tabindex="4">
    </fieldset>
    <?php echo validation_errors(); ?>
    <footer>

    </footer>
</div>
<footer id="main">
    <p>University of Colombo School of Computing</p>
</footer>

</body>
</html>