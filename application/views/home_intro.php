<br>
<br>
<div class="container">
    <div>
        <div class="col-md-4 panel panel-primary">
            <div class="panel-heading"><h3>What is AMS?</h3></div>
            <div class="panel-body"><p class="text-justify">AMS is an Audio Monitoring System. It can use as a solution to ongoing illegal broadcasting and copyright infringements. 
				It has facilities to store songs.</p></div>
        </div>
        <div class="col-md-4 panel panel-primary">
            <div class="panel-heading"><h3>Who can use this?</h3></div>
            <div class="panel-body"><p class="text-justify">OSCA staff can log on to the system to register/edit songs.
				Other users served as public users. They can't log on to the system. They can view songs and listen to part of songs. </p>
            </div></div>
        <div class="col-md-4 panel panel-primary">
            <div class="panel-heading"><h3>About OSCA</h3></div>
            <div class="panel-body"><p class="text-justify">OSCA is an entity dedicated to artistes whose creativity lies with that unique medium aesthetics –‘Song’.
				OSCA is a totally non-governmental and non-profit organization.</p>
            </div></div>
    </div>
</div>
</body>
</html>