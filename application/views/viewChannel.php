<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br><br><br><br><br>
            <h3>View Channels</h3>
			<br>
			
			<table class="table table-bordered">
            <tr>
                <th>Channel ID</th>
                <th>Channel Name</th>
				<th>Frequency</th>
                <th>Address Number</th>
				<th>Address Street</th>
				<th>Address City</th>
				<th>Postal Code</th>
            </tr>
            <?php
                foreach ($records as $key ) {
                    echo "<tr>"."<td>".$key->idChannel."</td>".
                            "<td>".$key->channelName."</td>".
							"<td>".$key->frequency."</td>".
                            "<td>".$key->channelAddressNumber."</td>".
                            "<td>".$key->channelAddressStreet."</td>".
							"<td>".$key->channelAddressCity."</td>".
							"<td>".$key->channelAddressPostalCode."</td>".
                            "</tr>";
                }
            ?>
			</table>


        </article>

