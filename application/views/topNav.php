<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
</head>
<body>
<div class="container">

    <header>

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <ul class="nav nav-tabs" role="tablist">

                    <li class='<?=($this->uri->segment(1)=="home")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/home') ?>">Home</a></li>
					<li class='<?=($this->uri->segment(1)=="about")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/about') ?>">About</a></li>
					<li class='<?=($this->uri->segment(1)=="song")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/song') ?>">Songs</a></li>
					<li class='<?=($this->uri->segment(1)=="channels")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/channels') ?>">Channels</a></li>
					<li class='<?=($this->uri->segment(1)=="artists")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/artists') ?>">Artists</a></li>
                    <li class='<?=($this->uri->segment(1)=="runsong")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/runsong') ?>">Match Record</a></li>
					<li class='<?=($this->uri->segment(1)=="Report")?"active":"nonactive"?>'><a href="<?php echo base_url('index.php/Report') ?>">View Report</a></li>
					

                    <li class="col-lg-offset-2 col-lg-1">
                       <button class="btn btn-primary navbar-btn" onclick="location.href='<?php echo base_url();?>index.php/profile'">Profile</button>
                    </li>
					<li class="col-lg-offset-1 col-lg-1">
                       <button class="btn btn-primary navbar-btn" onclick="location.href='<?php echo base_url();?>index.php/home/logout'">Logout</button>
                    </li>

                </ul>
            </div>
        </nav>


