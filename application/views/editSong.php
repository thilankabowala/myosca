<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Welcome OSCA</title>

    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-1.11.1.min.js') ?>"></script>
    <!-- Script for add more artist-->
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 3; //maximum input boxes allowed
            var wrapper         = $(".add_more_artist"); //Fields wrapper
            var add_button      = $(".add_artist_button"); //Add button ID
            text1 = '<div><select class="form-control" name="artist_name'
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append(text1+x+'"><option disabled selected>-----Artist of the Song-----</option> <?php foreach ($artist as $key ) { echo "<option>".$key->firstName." ".$key->middlName." ".$key->lastName."</option>"; }?></select><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });</script>

    <!-- Script for add more composer-->
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 3; //maximum input boxes allowed
            var wrapper         = $(".add_more_composer"); //Fields wrapper
            var add_button      = $(".add_composer_button"); //Add button ID
            text1 = '<div><select class="form-control" name="composer_name'
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append(text1+x+'"><option disabled selected>-----Composer of the Song-----</option> <?php foreach ($composer as $key ) { echo "<option>".$key->firstName." ".$key->middlName." ".$key->lastName."</option>"; }?></select><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });</script>

    <!-- Script for add more lyricist-->
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 3; //maximum input boxes allowed
            var wrapper         = $(".add_more_lyricist"); //Fields wrapper
            var add_button      = $(".add_lyricist_button"); //Add button ID
            text1 = '<div><select class="form-control" name="lyricist_name'
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append(text1+x+'"><option disabled selected>-----Lyricist of the Song-----</option> <?php foreach ($lyricist as $key ) { echo "<option>".$key->firstName." ".$key->middlName." ".$key->lastName."</option>"; }?></select><a href="#" class="remove_field">Remove</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });</script>

</head>
<body>
<div class="container">


    <div class="row">

        <article class="col-lg-9 col-lg-offset-1 col-lg-push-2">
            <br>
            <br>
            <br>
            <br>
            <br>
            <h3>Edit Song</h3>
            <div style="height:400px;line-height:3em;overflow:auto;padding:5px;">
                <form class="addition" role="form" action="<?php echo base_url();?>index.php/Song/updateSong" method="POST">

                    <div class="form-group ">
                        <label>Song Code</label>
                        </br>
                        <input type="text" class="form-control" name="song_code" value="<?php echo $records[0]->idsong;?>" required>

                    </div>

                    <div class="form-group ">
                        <label>Song Name</label>
                        </br>
                        <input type="text" class="form-control" name="song_name" value="<?php echo $records[0]->songName;?>" required>
                    </div>

                    <div class="form-group add_more_artist">
                        <label>Artist Name</label>
                        </br>
                        <select class="form-control" name="artist_name" required>
                            <option selected><?php echo $records[0]->afirstName." ".$records[0]->amiddlName." ".$records[0]->alastName;?></option>
                            <?php
                                foreach ($artist as $key ) {
                                 echo "<option>".$key->afirstName." ".$key->amiddlName." ".$key->alastName."</option>";
                                }
                            ?>

                        </select><button class="btn btn-default add_artist_button">+</button>

                    </div>

                    <div class="form-group add_more_composer">
                        <label>Composer Name</label>
                        </br>
                        <select class="form-control" name="composer_name" required>
                            <option selected><?php echo $records[0]->cfirstName." ".$records[0]->cmiddlName." ".$records[0]->clastName;?></option>
                            <?php
                            foreach ($composer as $key ) {
                                echo "<option>".$key->cfirstName." ".$key->cmiddlName." ".$key->clastName."</option>";
                            }
                            ?>

                        </select>
                        <button class="btn btn-default add_composer_button">+</button>

                    </div>

                    <div class="form-group add_more_lyricist">
                        <label>Lyricist Name</label>
                        </br>
                        <select class="form-control" name="lyricist_name" required>
                            <option selected><?php echo $records[0]->lfirstName." ".$records[0]->lmiddlName." ".$records[0]->llastName;?></option>
                            <?php
                            foreach ($lyricist as $key ) {
                                echo "<option>".$key->lfirstName." ".$key->lmiddlName." ".$key->llastName."</option>";
                            }
                            ?>

                        </select>
                        <button class="btn btn-default add_lyricist_button">+</button>
                    </div>

					<input type="hidden" class="form-control" name="song_id" value="<?php echo $records[0]->idsong;?>">
					<input type="hidden" class="form-control" name="old_matadata_id" value="<?php echo $records[0]->idmatadata;?>">

                    <button type="submit" class="btn btn-primary">Submit</button>

                </form>
            </div>
        </article>

