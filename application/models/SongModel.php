<?php
class SongModel extends CI_Model
{
	
	public function insertSong()
    {
        $songIdentifier = $_POST['song_code'];
        $songName = $_POST['song_name'];
        $artistName = explode (' ', $_POST['artist_name']);
        $composerName = explode (' ', $_POST['composer_name']);
        $lyricistName = explode (' ', $_POST['lyricist_name']);


        $q1 = $this->db->query("SELECT idArtist FROM artist WHERE afirstName = '$artistName[0]' AND amiddlName = '$artistName[1]' AND alastName = '$artistName[2]' ");
        $artistObject = $q1->result();
        $artistId = $artistObject[0];

        $q2 = $this->db->query("SELECT idComposer FROM composer WHERE cfirstName = '$composerName[0]' AND cmiddlName = '$composerName[1]' AND clastName = '$composerName[2]'");
        $composerObject = $q2->result();
        $composerId = $composerObject[0];


        $q3 = $this->db->query("SELECT idLyricist FROM lyricist WHERE lfirstName = '$lyricistName[0]' AND lmiddlName = '$lyricistName[1]' AND llastName = '$lyricistName[2]'");
        $lyricistObject = $q3->result();
        $lyricistId = $lyricistObject[0];
		
		
		$checkAvailable = mysql_query("SELECT idmatadata FROM matadata WHERE artist='$artistId->idArtist' AND composer ='$composerId->idComposer' AND lyricist = '$lyricistId->idLyricist' ");
        if(mysql_num_rows($checkAvailable)==0){
			$this->db->query("INSERT INTO matadata VALUES ('','$artistId->idArtist','$composerId->idComposer','$lyricistId->idLyricist')");
		}

        $this->db->query("INSERT INTO song  VALUES('','$songIdentifier','$songName','1')");

        $q4 = $this->db->query("SELECT idmatadata FROM matadata WHERE artist='$artistId->idArtist' AND composer ='$composerId->idComposer' AND lyricist = '$lyricistId->idLyricist' ");
        $matadataObject = $q4->result();
        $matadataID = $matadataObject[0];

        $q5 = $this->db->query("SELECT idsong FROM song WHERE code = '$songIdentifier' ");
        $songObject = $q5->result();
        $songId = $songObject[0];
        $sid=$songId->idsong;
        $res = $this->db->query("INSERT INTO songMatadata VALUES ('$sid','$matadataID->idmatadata')");
        return $res;

    }
	
	function getSongs() {
		
		$sql = "SELECT song.idsong, song.songName, matadata.*, artist.afirstName, artist.amiddlName, artist.alastName, 
		composer.cfirstName, composer.cmiddlName, composer.clastName, lyricist.lfirstName, lyricist.lmiddlName, lyricist.llastName FROM 
		((((songmatadata LEFT JOIN song ON songmatadata.idsong=song.idsong) LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
		LEFT JOIN artist ON (matadata.artist = artist.idArtist))
		LEFT JOIN composer ON (matadata.composer = composer.idComposer))
		LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist)";
		
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getSongNames() {

        $query=$this->db->get('song');
        $query=$this->db->query('SELECT * FROM song');
        return $query->result();
    }
	
	function getSongDetails($songName) {
		$query1 = $this->db->query("SELECT 	idsong FROM song WHERE songName= '$songName'");
		$songIdObject = $query1->result();
		$songId = $songIdObject[0]->idsong;
		
		$sql = "SELECT 	matadata.*, song.*, songmatadata.*, artist.*, composer.*, lyricist.* FROM 
		((((songmatadata LEFT JOIN song ON songmatadata.idsong=song.idsong) LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
		LEFT JOIN artist ON (matadata.artist = artist.idArtist))
		LEFT JOIN composer ON (matadata.composer = composer.idComposer))
		LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist)
		WHERE song.idsong = " . $songId;
			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	public function updateSong(){
		
		$songIdentifier = $_POST['song_code'];
        $songName = $_POST['song_name'];
        $artistName = explode (' ', $_POST['artist_name']);
        $composerName = explode (' ', $_POST['composer_name']);
        $lyricistName = explode (' ', $_POST['lyricist_name']);
		
		$SongID = $_POST['song_id'];
		$oldMatadatalID = $_POST['old_matadata_id'];
		
		$q1 = $this->db->query("SELECT idArtist FROM artist WHERE afirstName = '$artistName[0]' AND amiddlName = '$artistName[1]' AND alastName = '$artistName[2]' ");
        $artistObject = $q1->result();
        $artistId = $artistObject[0]->idArtist;

        $q2 = $this->db->query("SELECT idComposer FROM composer WHERE cfirstName = '$composerName[0]' AND cmiddlName = '$composerName[1]' AND clastName = '$composerName[2]'");
        $composerObject = $q2->result();
        $composerId = $composerObject[0]->idComposer;

        $q3 = $this->db->query("SELECT idLyricist FROM lyricist WHERE lfirstName = '$lyricistName[0]' AND lmiddlName = '$lyricistName[1]' AND llastName = '$lyricistName[2]'");
        $lyricistObject = $q3->result();
        $lyricistId = $lyricistObject[0]->idLyricist;
		
		$checkAvailable = mysql_query("SELECT idmatadata FROM matadata WHERE artist='$artistId' AND composer ='$composerId' AND lyricist = '$lyricistId' ");
        if(mysql_num_rows($checkAvailable)==0){
			$this->db->query("INSERT INTO matadata VALUES ('','$artistId','$composerId','$lyricistId')");
		}
		
		
		$q4 = $this->db->query("SELECT idmatadata FROM matadata WHERE artist='$artistId' AND composer ='$composerId' AND lyricist = '$lyricistId' ");
        $matadataObject = $q4->result();
        $newMatadataID = $matadataObject[0]->idmatadata;
		
		if($newMatadataID != $oldMatadatalID){
			$this->db->query("DELETE FROM songmatadata WHERE idsong=$SongID AND idmatadata=$oldMatadatalID ");
			$this->db->query("INSERT INTO songmatadata VALUES ($SongID, $newMatadataID) ");
		
			$checkToDeleteFromMatadata = mysql_query("SELECT idsong FROM songmatadata WHERE idmatadata='$oldMatadatalID' ");
			if(mysql_num_rows($checkToDeleteFromMatadata)==0){
				$this->db->query("DELETE FROM matadata WHERE idmatadata=$oldMatadatalID ");
			}
		}
		
		$res = $this->db->query("UPDATE song SET code='$songIdentifier', songName='$songName' WHERE idsong=$SongID");
		return $res;
	}

    function getArtist() {

        $query=$this->db->get('artist');
        $query=$this->db->query('SELECT * FROM artist');
        return $query->result();
    }
	
    function getComposer() {

        $query=$this->db->get('composer');
        $query=$this->db->query('SELECT * FROM composer');
        return $query->result();
    }
	
    function getLyricist() {

        $query=$this->db->get('lyricist');
        $query=$this->db->query('SELECT * FROM lyricist');
        return $query->result();
    }
	
	function deleteSong($songName) {
		
		$query1 = $this->db->query("SELECT 	idsong FROM song WHERE songName= '$songName'");
		$songIdObject = $query1->result();
		$songId = $songIdObject[0]->idsong;
			
		$query2 = $this->db->query("SELECT idmatadata FROM songmatadata LEFT JOIN song ON (songmatadata.idsong = song.idsong)
		WHERE song.idsong = " .$songId);
		$matadataIdObject = $query2->result();
		$matadataId = $matadataIdObject[0]->idmatadata;
			
		$sqlS = "DELETE FROM song WHERE idsong = " .$songId;
		$queryS = $this->db->query($sqlS);
		$sqlM = "DELETE FROM songmatadata WHERE idmatadata = " .$matadataId ;
		$query = $this->db->query($sqlM);
		
		$checkAvailable = mysql_query("SELECT idsong FROM songmatadata WHERE songmatadata.idmatadata = ".$matadataId);
        if(mysql_num_rows($checkAvailable)==0){
			$this->db->query("DELETE FROM matadata WHERE idmatadata = " .$matadataId);
		}
		
		if($query = null){
			return 1;
		}else{
			return 0;
		}
	}
	
	function getSongsByArtist() {
		$artistName = explode (' ', $_POST['artist_name']);
		$query1 = $this->db->query("SELECT idArtist FROM artist WHERE afirstName = '$artistName[0]' AND amiddlName = '$artistName[1]' AND alastName = '$artistName[2]'");
		$artistIdObject = $query1->result();
		$artistId = $artistIdObject[0]->idArtist;
		
		$sql = "SELECT 	matadata.*, song.*, songmatadata.*, artist.*, composer.*, lyricist.* FROM 
		((((songmatadata LEFT JOIN song ON songmatadata.idsong=song.idsong) LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
		LEFT JOIN artist ON (matadata.artist = artist.idArtist))
		LEFT JOIN composer ON (matadata.composer = composer.idComposer))
		LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist)
		WHERE artist.idArtist = '" .$artistId. "'";
			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getSongsByComposer() {
		$composertName = explode (' ', $_POST['composer_name']);
		$query1 = $this->db->query("SELECT idComposer FROM composer WHERE cfirstName = '$composertName[0]' AND cmiddlName = '$composertName[1]' AND clastName = '$composertName[2]'");
		$composerIdObject = $query1->result();
		$composerId = $composerIdObject[0]->idComposer;
		
		$sql = "SELECT 	matadata.*, song.*, songmatadata.*, artist.*, composer.*, lyricist.* FROM 
		((((songmatadata LEFT JOIN song ON songmatadata.idsong=song.idsong) LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
		LEFT JOIN artist ON (matadata.artist = artist.idArtist))
		LEFT JOIN composer ON (matadata.composer = composer.idComposer))
		LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist)
		WHERE composer.idComposer = '" .$composerId. "'";
			
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function getSongsByLyricist() {
		$lyricistName = explode (' ', $_POST['lyricist_name']);
		$query1 = $this->db->query("SELECT idLyricist FROM lyricist WHERE lfirstName = '$lyricistName[0]' AND lmiddlName = '$lyricistName[1]' AND llastName = '$lyricistName[2]'");
		$lyricistIdObject = $query1->result();
		$lyricistId = $lyricistIdObject[0]->idLyricist;
		
		$sql = "SELECT 	matadata.*, song.*, songmatadata.*, artist.*, composer.*, lyricist.* FROM 
		((((songmatadata LEFT JOIN song ON songmatadata.idsong=song.idsong) LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
		LEFT JOIN artist ON (matadata.artist = artist.idArtist))
		LEFT JOIN composer ON (matadata.composer = composer.idComposer))
		LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist)
		WHERE lyricist.idLyricist = '" .$lyricistId. "'";
			
		$query = $this->db->query($sql);
		return $query->result();
	}
}
?>