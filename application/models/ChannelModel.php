<?php

	class ChannelModel extends CI_Model{
	
		public function insertChannel()
		{
			$f1 = $_POST['channel_id'];
			$f2 = $_POST['channel_name'];
			$f3 = $_POST['channel_frequency'];
			$f4 = $_POST['address_num'];
			$f5 = $_POST['address_street'];
			$f6 = $_POST['address_city'];
			$f7 = $_POST['address_postal_code'];

			$this->db->query("INSERT INTO channel VALUES(0,'$f2','$f4','$f5','$f6','$f7')");
			
			$q2 = $this->db->query("SELECT idChannel FROM channel WHERE channelName = '$f2' ");
			$channelObject = $q2->result();
			$channelId = $channelObject[0];
			$cid=$channelId->idChannel;
			//var $res;
			$res = $this->db->query("INSERT INTO channel_frequencies VALUES ('$cid','$f3',0)");
			
			/* foreach ($f3 as $freq) {
				$quer="INSERT INTO channel_frequencies(channel_id, frequency) VALUES ('$cid','$freq')" ;
				$rs=mysql_query($quer);
				$num=mysql_num_rows($quer);
			} */
			//return $num;
			//$res= $this->db->query("INSERT INTO channel VALUES('$f1','$f2','$f4','$f5','$f6','$f7')");
			return $res;
		}
		
		public function updateChannel()
		{
			$f1 = $_POST['channel_id'];
			$f2 = $_POST['channel_name'];
			$f3 = $_POST['channel_frequency'];
			$f4 = $_POST['address_num'];
			$f5 = $_POST['address_street'];
			$f6 = $_POST['address_city'];
			$f7 = $_POST['address_postal_code'];
			
			$oldFrequencyID = $_POST['old_frequency_id'];
			$oldChannelID = $_POST['old_channel_id'];
			
			$this->db->query("UPDATE channel SET idChannel=$f1,channelName='$f2',channelAddressNumber='$f4',
			channelAddressStreet='$f5',channelAddressCity='$f6',channelAddressPostalCode=$f7 WHERE idChannel=$oldChannelID");
			
			$res = $this->db->query("UPDATE channel_frequencies SET channel_id=$f1, frequency='$f3' WHERE frequency_id=$oldFrequencyID ");
			return $res;
		}
		
		function getChannels() {
			
			$sql = "SELECT channel_frequencies.*, channel.* FROM 
			channel_frequencies LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)";
			
			$query = $this->db->query($sql);
			return $query->result();
		}
		
		function getChannel() {
		
			$this->db->select('channel.*');
			$this->db->from('channel');
			return $this->db->get()->result();
		}
		
		function getChannelDetails($channel) {
		
			$query1 = $this->db->query("SELECT 	idChannel FROM channel WHERE channelName= '$channel'");
			$channelIdObject = $query1->result();
			$channelId = $channelIdObject[0];
			
			$sql = "SELECT channel_frequencies.*, channel.* FROM 
			channel_frequencies LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)
			WHERE channel.idChannel = " . $channelId ->idChannel;
			
			$query = $this->db->query($sql);
			return $query->result();
		}
		
		function deleteChannel($channel) {
		
			$query1 = $this->db->query("SELECT 	idChannel FROM channel WHERE channelName= '$channel'");
			$channelIdObject = $query1->result();
			$channelId = $channelIdObject[0]->idChannel;
			
			$query2 = $this->db->query("SELECT 	frequency_id FROM channel_frequencies LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)
			WHERE channel.idChannel = " . $channelId);
			$frequncyIdObject = $query2->result();
			$frequncyId = $frequncyIdObject[0]->frequency_id;
			
			$sqlC = "DELETE FROM channel WHERE idChannel = " . $channelId;
			$queryC = $this->db->query($sqlC);
			$sqlF = "DELETE FROM channel_frequencies WHERE frequency_id = " . $frequncyId ;
			$query = $this->db->query($sqlF);
			if($query = null){
				return 1;
			}else{
				return 0;
			}
		}
		
		function getFrequency() {
		
			$this->db->select('channel_frequencies.*');
			$this->db->from('channel_frequencies');
			return $this->db->get()->result();
		}
		
		function getChannelByFrequency($frequency) {
		
			/* $query1 = $this->db->query("SELECT 	idChannel FROM channel WHERE channelName= '$channel'");
			$channelIdObject = $query1->result();
			$channelId = $channelIdObject[0] ->idChannel; */
			
			$sql = "SELECT channel_frequencies.*, channel.* FROM 
			channel_frequencies LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)
			WHERE channel_frequencies.frequency = " . $frequency;
			
			$query = $this->db->query($sql);
			return $query->result();
		}
	}
?>