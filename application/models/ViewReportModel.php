<?php

	class ViewReportModel extends CI_Model{
		
		function getFullReport() {
			
			$sql = "SELECT song.idsong, song.songName, matadata.*, report_data.*, songmatadata.*,
			channel_frequencies.*, channel.idChannel, channel.channelName FROM 
			(((((((report_data LEFT JOIN song ON report_data.song_id=song.idsong)
			LEFT JOIN songmatadata ON(song.idsong= songmatadata.idsong))
			LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
			LEFT JOIN artist ON (matadata.artist = artist.idArtist))
			LEFT JOIN composer ON (matadata.composer = composer.idComposer))
			LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist))
			LEFT JOIN channel_frequencies ON (report_data.frequency_id = channel_frequencies.frequency_id))
			LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)";
			
			$query = $this->db->query($sql);
			return $query->result();
		}
		
		function getReport($channel) {
		
			$query1 = $this->db->query("SELECT 	idChannel FROM channel WHERE channelName= '$channel'");
			$channelIdObject = $query1->result();
			$channelId = $channelIdObject[0];
			
			$sql = "SELECT song.idsong, song.songName, matadata.*, report_data.*, songmatadata.*,
			channel_frequencies.*, channel.idChannel, channel.channelName FROM 
			(((((((report_data LEFT JOIN song ON report_data.song_id=song.idsong)
			LEFT JOIN songmatadata ON(song.idsong= songmatadata.idsong))
			LEFT JOIN matadata ON(songmatadata.idmatadata= matadata.idmatadata))
			LEFT JOIN artist ON (matadata.artist = artist.idArtist))
			LEFT JOIN composer ON (matadata.composer = composer.idComposer))
			LEFT JOIN lyricist ON (matadata.lyricist = lyricist.idLyricist))
			LEFT JOIN channel_frequencies ON (report_data.frequency_id = channel_frequencies.frequency_id))
			LEFT JOIN channel ON (channel_frequencies.channel_id = channel.idChannel)
			WHERE channel.idChannel = " . $channelId ->idChannel;
			
			$query = $this->db->query($sql);
			return $query->result();
			
			
			/* $this->db->select('identifiedbroadcastings.*, channel.channelName, song.code, song.matadata, song.mp3file, matadata.artist, 
									matadata.composer, matadata.lyricist, mp3file.fileName, mp3file.filePath');
			$this->db->from('identifiedbroadcastings','channel','song','matadata','mp3file');
			
			$this->db->join('channel', 'identifiedbroadcastings.channelID= channel.idChannel');
			$this->db->join('song', 'identifiedbroadcastings.songID= song.idsong');
			$this->db->join('matadata', 'song.matadata= matadata.idmatadata');
			$this->db->join('mp3file', 'song.mp3file= mp3file.idmp3file');
				
			return $this->db->get()->result(); */
			
		}
		
		function getChannels() {
		
			$this->db->select('channel.*');
			$this->db->from('channel');
			
			return $this->db->get()->result();
		}
		
	}
?>