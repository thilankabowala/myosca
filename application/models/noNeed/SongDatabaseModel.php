<?php

class SongDatabaseModel extends CI_Model{
	
	function getData() {
        
        $this->db->select('song.*, matadata.artist, matadata.composer, matadata.lyricist, mp3file.fileName, mp3file.filePath');
		$this->db->from('song','matadata','mp3file');

		$this->db->join('matadata', 'song.matadata= matadata.idmatadata');
		$this->db->join('mp3file', 'song.mp3file= mp3file.idmp3file');

		return $this->db->get()->result();
	}
}
?>