<?php

class ArtistsModel extends CI_Model{
	
	public function insertArtist()
    {
        $f1 = $_POST['artist_id'];
        $f2 = $_POST['artist_fname'];
        $f3 = $_POST['artist_mname'];
        $f4 = $_POST['artist_lname'];
        $f5 = $_POST['artist_hnum'];
        $f6 = $_POST['artist_street'];
        $f7 = $_POST['artist_city'];

        $res= $this->db->query("INSERT INTO artist VALUES('$f1','$f2','$f3','$f4','$f5','$f6','$f7')");
        return $res;
    }

    public function insertComposer()
    {
        $f1 = $_POST['composer_id'];
        $f2 = $_POST['composer_fname'];
        $f3 = $_POST['composer_mname'];
        $f4 = $_POST['composer_lname'];
        $f5 = $_POST['composer_hnum'];
        $f6 = $_POST['composer_street'];
        $f7 = $_POST['composer_city'];

        $res= $this->db->query("INSERT INTO composer VALUES('$f1','$f2','$f3','$f4','$f5','$f6','$f7')");
        return $res;
    }

    public function insertLyricist()
    {
        $f1 = $_POST['lyricist_id'];
        $f2 = $_POST['lyricist_fname'];
        $f3 = $_POST['lyricist_mname'];
        $f4 = $_POST['lyricist_lname'];
        $f5 = $_POST['lyricist_hnum'];
        $f6 = $_POST['lyricist_street'];
        $f7 = $_POST['lyricist_city'];

        $res= $this->db->query("INSERT INTO lyricist VALUES('$f1','$f2','$f3','$f4','$f5','$f6','$f7')");
        return $res;
    }
	
	function getArtist() {
        
        $query=$this->db->get('artist');
        $query=$this->db->query('SELECT * FROM artist');
        return $query->result();
    }
	
	function getComposer() {
        
        $query=$this->db->get('composer');
        $query=$this->db->query('SELECT * FROM composer');
        return $query->result();
    }
	
	function getLyricist() {
        
        $query=$this->db->get('	lyricist');
        $query=$this->db->query('SELECT * FROM 	lyricist');
        return $query->result();
    }
}
?>