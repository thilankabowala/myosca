<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ViewSongController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->model('SongDatabaseModel');
	}

	public function index()
	{
		if(isset($_GET['q']) && $_GET['q']){ 
			$q = $this->input->get('q');
			$this->data['data']=$this->SongDatabaseModel->getSongsLiveSearch($q);
		}
		else{
			$this->data['data']=array_fill_keys(array('judul', 'contain'), NULL);
		}
		$this->data['body']='dashboard';
		$this->load->view('viewSong',$this->data);
	}

	public function live_search(){
		if($this->input->post(null)){
		   //put your search code here and just "echo" it out 
		}
	}
		
	$.ajax({
		url:'http://localhost/ams/controller/live_search',
		type : 'POST',
		data : { 'q' : escape($("#q").val()) },
		success: function (data){
			$("#result").html(data);
			allow = true;
		}
	});
}

?>

/* End of file ViewSongController.php */
/* Location: ./application/controllers/ViewSongController.php */