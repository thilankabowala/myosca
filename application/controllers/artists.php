<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI
class artists extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addArtist');
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    

    function addArtist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addArtist');
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function insertArtist()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ArtistsModel');
            $data2 = $this->ArtistsModel->insertArtist();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('insertError',$data);
                $this->load->view('sideNavArtists');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('success');
                $this->load->view('sideNavArtists');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    function addComposer()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addComposer');
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function insertComposer()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ArtistsModel');
            $data2 = $this->ArtistsModel->insertComposer();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('insertError',$data);
                $this->load->view('sideNavArtists');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('success');
                $this->load->view('sideNavArtists');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    function addLyricist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addLyricist');
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function insertLyricist()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ArtistsModel');
            $data2 = $this->ArtistsModel->insertLyricist();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('insertError',$data);
                $this->load->view('sideNavArtists');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('success');
                $this->load->view('sideNavArtists');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    function viewArtist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ArtistsModel');
			$data['records']=$this->ArtistsModel->getArtist();
            $this->load->view('viewArtist',$data);
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function viewComposer()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ArtistsModel');
			$data['records']=$this->ArtistsModel->getComposer();
            $this->load->view('viewComposer',$data);
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function viewLyricist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ArtistsModel');
			$data['records']=$this->ArtistsModel->getLyricist();
            $this->load->view('viewLyricist',$data);
            $this->load->view('sideNavArtists');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
}
?>