<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI
class About extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('about');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
}
?>