<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI

class Report extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }
    
	function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ViewReportModel');
			$data['records']=$this->ViewReportModel->getFullReport();
            $this->load->view('ViewReport',$data);
			$this->load->view('sideNavReports');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewFullReport()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ViewReportModel');
			$data['records']=$this->ViewReportModel->getFullReport();
            $this->load->view('ViewReport',$data);
			$this->load->view('sideNavReports');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function reportByChannel()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ViewReportModel');
			$data['records']=$this->ViewReportModel->getChannels();
            $this->load->view('generateReportByChannel',$data);
			$this->load->view('sideNavReports');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewReportByChannel()
    {
        if($this->session->userdata('logged_in'))
        {
			$channel = $_POST["channel_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ViewReportModel');
			$data['records']=$this->ViewReportModel->getReport($channel);
            $this->load->view('ViewReport',$data);
			$this->load->view('sideNavReports');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

}
?>