<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI
class Runsong extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('checkrec');
			//$this->load->view('sideNav');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    function generateHashTag()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];

            $file  = $_FILES['fileToGenerateHashTag'];
            $filename  = $_FILES['fileToGenerateHashTag']['name'];
            //$filepath  = realpath($_FILES['fileToGenerateHashTag']['tmp_name']);

            $inputDir  = "D:\\waki\AudioTracking";
            $FILE_UPLOADED_PATH = "D:\Waki\mp3";
            $uploadOk = 1;

            //$fileType = $_FILES['fileToGenerateHashTag']['type'];
            //echo "File type: ".$fileType."<br/>";
            $theFileType = pathinfo($filename,PATHINFO_EXTENSION);
            //echo "File type: ".$theFileType."<br/>";

            // Check if file is an audio
            if(($theFileType != "audio/mpeg") || ($theFileType != "audio/mp4") || ($theFileType != "audio/mp3"))
            {

                echo "File is not an audio.<br/>";
                //$uploadOk = 0;
            }

            // Check if audio file is a actual audio or fake audio
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToGenerateHashTag"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an audio - " . $check["mime"] . ".<br/>";
                    $uploadOk = 1;
                } else {
                    echo "File is not an audio in size.<br/>";
                    //$uploadOk = 0;
                }
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {

                echo 'Sorry, your file was not uploaded';

            } else {
                if(isset($file)) {
                    $command = "matlab -sd ".$inputDir." -r directMatch('".$FILE_UPLOADED_PATH."\\".$filename."')";
                    exec($command);
                    //echo "The following command was run: ".$command."<br/>";
                    //echo "Hash tag was generated for ".$filename."<br/>";

                } else {
                    echo "Sorry, there was an error while generating hash tag for your file.<br/>";

                }
            }
            $this->load->view('topNav', $data);
            $this->load->view('matchRecord');
            //$this->load->view('sideNav');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
}
?>