<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI
class Channels extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addChannel');
            $this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    
	
	function addChannel()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->view('addChannel');
            $this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function insertChannel()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ChannelModel');
            $data2 = $this->ChannelModel->insertChannel();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('insertError',$data);
                $this->load->view('sideNavChannels');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('success');
                $this->load->view('sideNavChannels');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
	
	function editChannel()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannel();
            $this->load->view('selectChannel',$data);
			$this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function editSelectedChannel()
    {
        if($this->session->userdata('logged_in'))
        {
			$channel = $_POST["channel_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannelDetails($channel);
            $this->load->view('editChannel',$data);
			$this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function updateChannel()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ChannelModel');
            $data2 = $this->ChannelModel->updateChannel();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('updateError',$data);
                $this->load->view('sideNavChannels');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('successUpdate');
                $this->load->view('sideNavChannels');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
	
	function deleteChannel()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannel();
            $this->load->view('deleteChannel',$data);
			$this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function deleteSelectedChannel()
    {
        if($this->session->userdata('logged_in'))
        {
			$channel = $_POST["channel_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('ChannelModel');
            $data2 = $this->ChannelModel->deleteChannel($channel);
            if ($data2 = 1) {
                $this->load->view('topNav', $data);
                $this->load->view('successDelete');
                $this->load->view('sideNavChannels');
            }
            else{
				$msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('deleteError',$data);
                $this->load->view('sideNavChannels');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

	function viewChannel()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannels();
            $this->load->view('viewChannel',$data);
            $this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function selectChannelToView()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannel();
            $this->load->view('selectChannelToView',$data);
			$this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewSpecificChannel()
    {
        if($this->session->userdata('logged_in'))
        {
			$channel = $_POST["channel_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannelDetails($channel);
            $this->load->view('viewChannel',$data);
            $this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function selectFrequencyToView()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getFrequency();
            $this->load->view('selectFrequencyToView',$data);
			$this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewChannelByFrequency()
    {
        if($this->session->userdata('logged_in'))
        {
			$frequency = $_POST["frequency"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('ChannelModel');
			$data['records']=$this->ChannelModel->getChannelByFrequency($frequency);
            $this->load->view('viewChannel',$data);
            $this->load->view('sideNavChannels');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
}
?>