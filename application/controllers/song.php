<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); we need to call PHP's session object to access it through CI
class Song extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
            $this->load->model('SongModel');
            $data['artist']=$this->SongModel->getArtist();
            $data['composer']=$this->SongModel->getComposer();
            $data['lyricist']=$this->SongModel->getLyricist();
            $this->load->view('addSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
    function add()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);

            $this->load->model('SongModel');
            $data['artist']=$this->SongModel->getArtist();
            $data['composer']=$this->SongModel->getComposer();
            $data['lyricist']=$this->SongModel->getLyricist();

            $this->load->view('addSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function insertSong()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('SongModel');
            $data2 = $this->SongModel->insertSong();

            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('insertError',$data);
                $this->load->view('sideNavSong');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('addmp3');
                $this->load->view('sideNavSong');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
	
	function generateHashTag()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];

            $file  = $_FILES['fileToGenerateHashTag'];
            $filename  = $_FILES['fileToGenerateHashTag']['name'];
            //$filepath  = realpath($_FILES['fileToGenerateHashTag']['tmp_name']);

            $inputDir  = "D:\\waki\AudioTracking";
            $FILE_UPLOADED_PATH = "D:\Waki\mp3";
            $uploadOk = 1;

            //$fileType = $_FILES['fileToGenerateHashTag']['type'];
            //echo "File type: ".$fileType."<br/>";
            $theFileType = pathinfo($filename,PATHINFO_EXTENSION);
            //echo "File type: ".$theFileType."<br/>";

            // Check if file is an audio
            if(($theFileType != "audio/mpeg") || ($theFileType != "audio/mp4") || ($theFileType != "audio/mp3"))
            {

                echo "File is not an audio.<br/>";
                //$uploadOk = 0;
            }

            // Check if audio file is a actual audio or fake audio
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["fileToGenerateHashTag"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an audio - " . $check["mime"] . ".<br/>";
                    $uploadOk = 1;
                } else {
                    echo "File is not an audio in size.<br/>";
                    //$uploadOk = 0;
                }
            }

            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {

                echo "Sorry, your file was not uploaded";
                
            } else {
                if(isset($file)) {
					$songId = 10;
                    $command = "matlab -sd ".$inputDir." -r Reg_mysong('".$FILE_UPLOADED_PATH."\\".$filename."',".$songId.")";
                    exec($command);
                    //echo "The following command was run: ".$command."<br/>";
                    //echo "Hash tag was generated for ".$filename."<br/>";

                } else {
                    echo "Sorry, there was an error while generating hash tag for your file.<br/>";
                    
                }
            }
            $this->load->view('topNav', $data);
            $this->load->view('success');
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function selectSongToEdit()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongNames();
            $this->load->view('selectSong',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
    function editSelectedSong()
    {
		if($this->session->userdata('logged_in'))
        {
			$songName = $_POST["song_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);

            $this->load->model('SongModel');
            $data['artist']=$this->SongModel->getArtist();
            $data['composer']=$this->SongModel->getComposer();
            $data['lyricist']=$this->SongModel->getLyricist();
            $data['records']=$this->SongModel->getSongDetails($songName);

            $this->load->view('editSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function updateSong()
    {
        if($this->session->userdata('logged_in'))
        {

            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('SongModel');
            $data2 = $this->SongModel->updateSong();
            if (!$data2) {
                // if query returns null
                $msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('updateError',$data);
                $this->load->view('sideNavSong');
            }
            else{
                $this->load->view('topNav', $data);
                $this->load->view('successUpdate');
                $this->load->view('sideNavSong');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
	
	function selectSongToDelete()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongNames();
            $this->load->view('deleteSong',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function deleteSelectedSong()
    {
        if($this->session->userdata('logged_in'))
        {
			$songName = $_POST["song_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->model('SongModel');
            $data2 = $this->SongModel->deleteSong($songName);
            if ($data2 = 1) {
                $this->load->view('topNav', $data);
                $this->load->view('successDelete');
                $this->load->view('sideNavSong');
            }
            else{
				$msg = $this->db->_error_message();
                $num = $this->db->_error_number();

                $data['msgdb'] = "Error(".$num.") ".$msg;
                $this->load->view('topNav', $data);
                $this->load->view('deleteError',$data);
                $this->load->view('sideNavSong');
            }
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    function viewSong()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongs();
            $this->load->view('viewSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function selectSongToView()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongNames();
            $this->load->view('selectSongToView',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewSpecificSong()
    {
        if($this->session->userdata('logged_in'))
        {
			$songName = $_POST["song_name"];
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongDetails($songName);
            $this->load->view('viewSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh'); 
        }
    }
	
	function selectArtistToViewSong()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['artist']=$this->SongModel->getArtist();
            $this->load->view('selectArtistToViewSong',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewSongsByArtist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongsByArtist();
            $this->load->view('viewSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh'); 
        }
    }
	
	function selectComposerToViewSong()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
            $data['composer']=$this->SongModel->getComposer();
            $this->load->view('selectComposerToViewSong',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewSongsByComposer()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongsByComposer();
            $this->load->view('viewSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh'); 
        }
    }
	
	function selectLyricistToViewSong()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
            $data['lyricist']=$this->SongModel->getLyricist();
            $this->load->view('selectLyricistToViewSong',$data);
			$this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
	
	function viewSongsByLyricist()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            $this->load->helper('html');
            $this->load->view('topNav', $data);
			$this->load->model('SongModel');
			$data['records']=$this->SongModel->getSongsByLyricist();
            $this->load->view('viewSong',$data);
            $this->load->view('sideNavSong');
        }
        else
        {
            //If no session, redirect to login page
            redirect('login', 'refresh'); 
        }
    }
}
?>